/**
   @file    UdpCli.c
   @author  Wei-Ying Tsai
   @contact wetsai@cs.stonybrook.edu
   @date 2015.10.29
*/

#include <unistd.h>
#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include <setjmp.h>
#include "unp.h"
#include "ArgumentsParser.h"
#include "ClientTasks.h"
#include "CommonTasks.h"

#ifdef DEBUG_UDP_CLIENT
#undef DEBUG_UDP_CLIENT
#endif
#ifdef DEBUG_DEADLOCK
#undef DEBUG_DEADLOCK
#endif
#ifdef OUTPUT_DATA_TO_CONSOLE
#undef OUTPUT_DATA_TO_CONSOLE
#endif
#ifdef OUTPUT_DATA_TO_FILE
#undef OUTPUT_DATA_TO_FILE
#endif

// Debug detailed client info
// Uncomment it if you want to debug
//#define DEBUG_UDP_CLIENT
//#define DEBUG_DEADLOCK


#define OUTPUT_DATA_TO_CONSOLE
//#define OUTPUT_DATA_TO_FILE

static const char  *OUTPUT_FILENAME = "result.txt";
static const unsigned int MAX_NETWORK_REC = 64;

pthread_mutex_t g_datagram_lock = PTHREAD_MUTEX_INITIALIZER;
pthread_cond_t g_datagram_cond = PTHREAD_COND_INITIALIZER;

static clientDatagram *g_datagrams;
static unsigned int g_ack;
static unsigned int advertisedWnd;
static int LFR;
static int LAF;
static unsigned int g_IsEOFRecived;
static BOOL g_AllAreAcked = FALSE;

static unsigned int g_connecitonPhaseTimeout = 0;
const static unsigned int g_MaxNoResponseTimeAllowed = 10;
const static unsigned int g_TimeWaitInSec = 30;

static sigjmp_buf jmpbuf;
static void sigAlarmHandler( int signo );

struct param
{
    clientArgs cliArgs;
    int datagramCount;
};

static void *ReadFileThreadHandler( void *arg );
static void IncrementBufferIndex( int *index, int maxSize );

int EstablishFileTransferConnection
    (
    int sockfd,
    char *requestFilename,
    int *newServPort
    );

void ReceiveFile
    (
    int sockfd,
    clientArgs *aArgs,
    const unsigned int datagramCount
    );


//=============================================================================
int main( int argc, char *argv[] )
{
    if( argc != 2 )
    {
        err_quit( "Usage: client client.in" );
    }

    clientArgs args;
    bzero( &args, sizeof( args ) );
    ReadClientData( argv[1], &args );
    DebugClientData( args );

    networkInterfaceRecord records[MAX_NETWORK_REC];
    unsigned int numOfNetworkRecords = Init( &records[0], MAX_NETWORK_REC, args );
    DebugClientRecords( records, numOfNetworkRecords );

    Signal( SIGALRM, sigAlarmHandler );

    // Compute IPServer and IPClient, which designates the IP address
    // the client will use to identify the server and will choose to identify
    // itself, respectively.
    char IPClient[MAXLINE];
    char IPServer[MAXLINE];

    bzero( IPClient, MAXLINE );
    bzero( IPServer, MAXLINE );
    srvCliRelation relation = InitIPServerAndIPClient
                                (
                                &records[0],
                                numOfNetworkRecords,
                                IPClient,
                                IPServer,
                                args.IPAddr
                                );

    int sockfd;
    struct sockaddr_in cliaddr;
    bzero( &cliaddr, sizeof( cliaddr ) );
    cliaddr.sin_family = AF_INET;
    cliaddr.sin_port = htons( 0 );  // Let kernel determine the port number
    Inet_pton( AF_INET, IPClient, &cliaddr.sin_addr );

    sockfd = Socket( AF_INET, SOCK_DGRAM, 0 );
    if( Local == relation || LoopBack == relation )
    {
    #ifdef DEBUG_UDP_CLIENT
        int optVal = 0;
        int optValLen = 0;
        getsockopt( sockfd, SOL_SOCKET, SO_DONTROUTE, &optVal , &optValLen );
        printf( "SO_DONTROUTE is :%d\n", optVal );
    #endif
        int setValue = 1;
        setsockopt( sockfd, SOL_SOCKET, SO_DONTROUTE, &setValue, sizeof(setValue) );
    #ifdef DEBUG_UDP_CLIENT
        printf( "Local: set SO_DONTROUTE.\n" );
        getsockopt( sockfd, SOL_SOCKET, SO_DONTROUTE, &optVal , &optValLen );
        printf( "SO_DONTROUTE is :%d\n", optVal );
    #endif
    }

    Bind( sockfd, (SA *) &cliaddr, sizeof( cliaddr ) );
    // Retrieve the client IP address and port number set by the kernel.
    socklen_t cliaddrLen = sizeof(cliaddr);
    if( getsockname( sockfd, (SA *)&cliaddr, &cliaddrLen ) < 0 )
    {
        err_ret("getsockname error");
    }
    printf("bound %s\n", Sock_ntop((SA *) &cliaddr, sizeof(cliaddr)) );

    // Specify server address structure
    struct sockaddr_in servaddr;
    bzero( &servaddr, sizeof( servaddr ) );
    servaddr.sin_family = AF_INET;
    servaddr.sin_port = args.portNum;
    Inet_pton( AF_INET, IPServer, &servaddr.sin_addr );
    printf("Connect to: %s\n", Sock_ntop((SA *) &servaddr, sizeof(servaddr)) );
    Connect( sockfd, (SA *) &servaddr, sizeof( servaddr ) );
    struct sockaddr_in peeraddr;
    socklen_t peeraddrLen = sizeof( peeraddr );
    bzero( &peeraddr, sizeof( peeraddr ) );
    if( getpeername( sockfd, (SA *)&peeraddr, &peeraddrLen ) < 0 )
    {
        err_ret("getpeername error");
    }
    printf("Peer: %s\n", Sock_ntop((SA *) &peeraddr, sizeof(peeraddr)) );

    int newServPort = 0;
    EstablishFileTransferConnection( sockfd, args.requestFilename, &newServPort );

    // Reuse the server socket address but change the port number
    // to re-connect.
    servaddr.sin_port = htons( newServPort );
    Connect( sockfd, (SA *) &servaddr, sizeof( servaddr ) );
    printf( "Reconnect to %s\n", Sock_ntop((SA *) &servaddr, sizeof(servaddr)) );
    Write( sockfd, "ok", strlen( "ok" ) );

    // Now the file transfer connection has been setup
    // The client starts to receive file from the server.

    pthread_t tid;

    unsigned int i = 0;
    unsigned int datagramCount = MAX_NUMBER_OF_BUFFER;
    struct param threadParam;
    threadParam.cliArgs = args;
    threadParam.datagramCount = datagramCount;
    g_ack = 0; // the seq number associated with the next expected datagram
    LFR = datagramCount - 1;
    LAF = args.maxSlidingWindowSize - 1;
    advertisedWnd = args.maxSlidingWindowSize;
    g_IsEOFRecived = 0;
    g_datagrams = (clientDatagram *)Calloc( datagramCount, sizeof( clientDatagram ) );
    Pthread_create( &tid, NULL, &ReadFileThreadHandler, &threadParam );
    printf( "Now start to receive file %s...\n", args.requestFilename );

    ReceiveFile( sockfd, &args, datagramCount );

    Pthread_join( tid, NULL );

    // Free the memory
    if( NULL != g_datagrams )
    {
        free( g_datagrams );
    }
}

int EstablishFileTransferConnection
    (
    int sockfd,
    char *requestFilename,
    int *newServPort
    )
{
    g_connecitonPhaseTimeout = 1;
    char sendline[MAXLINE];
    char recvline[MAXLINE];
    bzero( &sendline, MAXLINE );
    bzero( &recvline, MAXLINE );
    int nRead = 0;

    // Time out variables
    struct rtt_info rttInfo;
    rtt_init( &rttInfo );
    rtt_d_flag = 0; // don't show additional info
    rtt_newpack( &rttInfo );
    while( nRead <= 0 )
    {
        printf( "Request %s from the server.\n", requestFilename );
        Write( sockfd, requestFilename, strlen( requestFilename ) );
        while( 1 )
        {
            StartTimer( &rttInfo );
            if( 0 != sigsetjmp( jmpbuf, 1 ) )
            {
                if( rtt_timeout( &rttInfo ) < 0 )
                {
                    err_sys( "No response from server after sending requested file name. Give up." );
                    errno = ETIMEDOUT;
                    return (-1);
                }

                char buf[MAXLINE];
                bzero( &buf, MAXLINE );
                time_t ticks = time( NULL );
                snprintf( buf, MAXLINE, "%s", ctime( &ticks ) );
                printf( "Socket time out: no response after sending file request.\n", buf );
                printf( "Re-send request %s file from the server.\n", requestFilename );
                break;
            }

            nRead = read( sockfd, recvline, MAXLINE );
            recvline[ nRead ] = 0; // null-terminate
            if( nRead > 0 )
            {
                // Happy! Receive response from server
                break;
            }
        }
    }

    StopTimer( &rttInfo, 0 );

    if( 0 == bcmp( recvline, "Port:", 5) )
    {
        *newServPort = atoi( recvline + 5 );
        printf( "Receive new port #%d\n", *newServPort );
    }

    g_connecitonPhaseTimeout = 0;
    return 1;
}

void ReceiveFile
    (
    int sockfd,
    clientArgs *aArgs,
    const unsigned int datagramCount
    )
{
    clientArgs args = *aArgs;
    srandom(args.seed);

    char recvBuf[SERVER_DATAGRAM_TOTAL_SIZE];
    char sendBuffer[CLIENT_DATAGRAM_TOTAL_SIZE];
    char serverPayload[SERVER_CLIENT_DATAGRAM_PAYOLAD_SIZE];
    int seqNum = 0;
    uint32_t timestamp = 0;
    BOOL localAllAreAcked = FALSE;
    unsigned int lastSeqNum = 0;
    fd_set rset1, allrset1;
    FD_ZERO(&rset1);
    FD_ZERO(&allrset1);
    FD_SET(sockfd, &allrset1);
    int maxfd1 = max( sockfd, 0 );
    unsigned int datagramReceived = 0;

    struct timeval timeOut;
    timeOut.tv_sec = g_MaxNoResponseTimeAllowed;
    while( !localAllAreAcked )
    {
        rset1 = allrset1;
        int nReady = MySelect( maxfd1 + 1, &rset1, NULL, NULL, &timeOut );
        if( FD_ISSET( sockfd, &rset1 ) )
        {
            datagramReceived = 1;
        }
        if( 0 == nReady )
        {
            if( 0 == datagramReceived )
            {
                printf( "No datagrams received in 20 seconds. Exit\n" );
                exit( 1 );
            }
            else
            {
                datagramReceived = 0;
            }
            continue;
        }
        bzero( &recvBuf, SERVER_DATAGRAM_TOTAL_SIZE );
        bzero( &sendBuffer, CLIENT_DATAGRAM_TOTAL_SIZE );
        bzero( &serverPayload, SERVER_CLIENT_DATAGRAM_PAYOLAD_SIZE );
        int nRead = Read( sockfd, recvBuf, SERVER_DATAGRAM_TOTAL_SIZE );
        //printf("nRead: %d\n", nRead);

        double givenProb = args.dataLossProb;
        double prob = ( (double)random() ) / MAX_RANDOM_NUMBER;
        if( prob < givenProb || givenProb == 1.0 )
        {
            printf("Decide to discard this datagram\n");
            continue;
        }

        if( 0 == bcmp( recvBuf, "Port:", 5 ) )
        {
            printf( "Receive new port #%d\n", atoi(recvBuf+5) );
            MyWrite( sockfd, "ok", strlen( "ok" ), args.dataLossProb );
            continue;
        }

    #ifdef DEBUG_DEADLOCK
        printf( "Main thread tries to enter\n" );
    #endif
        Pthread_mutex_lock( &g_datagram_lock );
    #ifdef DEBUG_DEADLOCK
        printf( "Main thread finally enters\n" );
    #endif
        if( 0 == bcmp( recvBuf, WINDOW_PROBE_STRING, strlen( WINDOW_PROBE_STRING ) ) )
        {
            //printf( "Receive window probe:%s\n", WINDOW_PROBE_STRING );
            char tempBuf[SERVER_DATAGRAM_TOTAL_SIZE];
            sprintf( tempBuf, "AWND:%u", advertisedWnd );
            MyWrite( sockfd, tempBuf, SERVER_DATAGRAM_TOTAL_SIZE, args.dataLossProb );

            // Don't forget to release the lock because you have
            // no way to obtain the key later...
            Pthread_cond_signal( &g_datagram_cond );
            Pthread_mutex_unlock( &g_datagram_lock );
            continue;
        }

        ParseServerDatagram( &seqNum, &timestamp, serverPayload, recvBuf );

        int discardThisDatagram = 0;
        if( ( LFR + args.maxSlidingWindowSize ) >= datagramCount )
        {
            if( LFR == datagramCount - 1 && seqNum > LAF )
            {
                // The message is outside the window, discard it.
                discardThisDatagram = 1;
            }
            if( seqNum > LAF && seqNum <= LFR )
            {
                // The message is outside the window, discard it.
                discardThisDatagram = 1;
            }
        }
        else
        {
            if( seqNum <= LFR || seqNum > LAF )
            {
                // The message is outside the window, discard it.
                discardThisDatagram = 1;
            }
        }

        if( 0 == discardThisDatagram && FALSE == ClientDatagramExists( g_datagrams, seqNum ) )
        {
            SetClientDatagram( &g_datagrams[seqNum], TRUE, timestamp, serverPayload );
            //printf( "(ts:%d) Receive seq #%d is stored to buffer: %s\n", timestamp, seqNum, g_datagrams[seqNum].data );
            printf( "(ts:%d) Seq #%d is stored to buffer.\n", timestamp, seqNum );
            --advertisedWnd;
        }
        else
        {
            // Here, the received datagram is either outside the window or
            // have already been buffered at the client. Needn't process it.
            printf( "(ts:%d) Receive seq #%d is outdated.\n", timestamp, seqNum, g_datagrams[seqNum].data );
        }

        g_ack = FindAccumulativeAck
                        (
                        g_datagrams,
                        LFR,
                        LAF,
                        datagramCount
                        );

        unsigned int sentTimestamp = ( 0 == g_ack )?
                                            g_datagrams[datagramCount].timestamp :
                                            g_datagrams[g_ack-1].timestamp;

        BuildClientCommBuffer
            (
            sendBuffer,
            g_ack,
            advertisedWnd,
            sentTimestamp
            );

        if( recvBuf[SERVER_DATAGRAM_TOTAL_SIZE-2] == '\0' )
        {
            //printf("======1======\n");
            lastSeqNum = seqNum;
            g_IsEOFRecived = 1;
        }

        if( nRead < SERVER_DATAGRAM_TOTAL_SIZE )
        {
            //printf("======2======\n");
            g_IsEOFRecived = 1;
        }

        if( 1 == g_IsEOFRecived
            && ( lastSeqNum + 1 ) % datagramCount == g_ack )
        {
            //printf("======3======\n");
            g_AllAreAcked = TRUE;
            localAllAreAcked = TRUE;
        }

        if( 0 == advertisedWnd )
        {
            printf("Window size = %d; locked\n", advertisedWnd );
        }
        BOOL written = MyWrite( sockfd, sendBuffer, CLIENT_DATAGRAM_TOTAL_SIZE, args.dataLossProb );
        if( TRUE == written )
        {
            printf("Send Ack(%u), advertiszed window (%u), timestamp(%u) to the server\n", g_ack, advertisedWnd, sentTimestamp );
        }

        //Pthread_cond_signal( &g_datagram_cond );
        Pthread_mutex_unlock( &g_datagram_lock );
    }

    // Now the sender complete sending the file.
    // We wait for a while and to see if there is still
    // so incoming retransmitted data.
    printf( "File is received. Now enter time wait state (30 sec)\n" );
    struct timeval timeVal;
    timeVal.tv_sec = g_TimeWaitInSec;
    fd_set rset, allrset;
    FD_ZERO(&allrset);
    FD_SET(sockfd, &allrset);
    int maxfd = max( sockfd, 0 );
    while( 1 )
    {
        rset = allrset;
        int nReady = MySelect( maxfd + 1, &rset, NULL, NULL, &timeVal );
        if( FD_ISSET( sockfd, &rset ) )
        {
            char sendBuffer[CLIENT_DATAGRAM_TOTAL_SIZE];
            char recvBuffer[CLIENT_DATAGRAM_TOTAL_SIZE];
            bzero( &sendBuffer, CLIENT_DATAGRAM_TOTAL_SIZE );
            bzero( &recvBuffer, CLIENT_DATAGRAM_TOTAL_SIZE );

            read( sockfd, recvBuffer, CLIENT_DATAGRAM_TOTAL_SIZE );
            // If we are here, all data has been received. But the
            // ack is lost so the server retransmit. Here, we just
            // send back ack; no need to store it into buffer again.
            double givenProb = args.dataLossProb;
            double prob = ( (double)random() ) / MAX_RANDOM_NUMBER;
            if( prob < givenProb )
            {
                printf("Decide to discard this datagram\n");
                continue;
            }

            ParseServerDatagram( &seqNum, &timestamp, serverPayload, recvBuf );
            Pthread_mutex_lock( &g_datagram_lock );
            if( ( seqNum + 1 ) % datagramCount == g_ack)
            {
                //unsigned int sentTimestamp = ( 0 == g_ack? )
                BuildClientCommBuffer
                    (
                    sendBuffer,
                    g_ack,
                    advertisedWnd,
                    timestamp
                    );

                if( 0 == advertisedWnd )
                {
                    printf("Window size = %d; locked\n", advertisedWnd );
                }

                BOOL written = MyWrite( sockfd, sendBuffer, CLIENT_DATAGRAM_TOTAL_SIZE, args.dataLossProb );
                if( TRUE == written )
                {
                    printf("Send Ack(%u), advertiszed window (%u), timestamp(%u) to the server\n", g_ack, advertisedWnd, timestamp );
                }
            }
            Pthread_mutex_unlock( &g_datagram_lock );
        }

        if( 0 == nReady )
        {
            // Time out; just exit.
            break;
        }
    }
}


/*static*/ void *
ReadFileThreadHandler( void *arg )
{
#ifdef OUTPUT_DATA_TO_FILE
    FILE *fp = Fopen( OUTPUT_FILENAME, "wb" );
#endif
    struct param *threadParam = (struct param *) arg;
    clientArgs args = threadParam->cliArgs;
    const int datagramCount = threadParam->datagramCount;
    unsigned int charCount = 0;

    while( 1 )
    {
    #ifdef DEBUG_DEADLOCK
        printf( "Thread tries to enter\n" );
    #endif

        Pthread_mutex_lock( &g_datagram_lock );

    #ifdef DEBUG_DEADLOCK
        printf( "Thread finally enters\n" );
    #endif
        //Pthread_cond_wait( &g_datagram_cond, &g_datagram_lock );
        unsigned int i = ( LFR + 1 ) % datagramCount;
        //printf("Before: LFR=%d, LAF=%d, i=%d\n", LFR, LAF, i);
        int currentSeqNum = 0;
        if( (LFR != datagramCount - 1) && LFR > LAF )
        {
            for( ; i < ( LAF + datagramCount ); ++i )
            {
                unsigned int index = i % datagramCount;
                if( TRUE == g_datagrams[index].valid )
                {
                    currentSeqNum = index;
                    if( '\0' == g_datagrams[index].data[0] )
                    {
                        break;
                    }
                    ++advertisedWnd;
                    if( '\0' == g_datagrams[i].data[0] )
                        continue;
                #ifdef OUTPUT_DATA_TO_CONSOLE
                    printf("Thread reads Seq #%d: %s\n", currentSeqNum, g_datagrams[index].data );
                #else
                    printf("Thread reads Seq #%d\n", currentSeqNum );
                #endif
                    //printf("%s\n", g_datagrams[index].data );
                #ifdef OUTPUT_DATA_TO_FILE
                    fprintf( fp, "%s", g_datagrams[index].data );
                #endif
                    charCount += strlen( g_datagrams[index].data );
                    InvalidateClientDatagram( &g_datagrams[index] );
                }
                else
                {
                    // Current index points to an out-of-order datagram.
                    break;
                }
            }
        }
        else
        {
            for( ; i <= LAF; ++i )
            {
                if( TRUE == g_datagrams[i].valid  )
                {
                    currentSeqNum = i;
                    if( '\0' == g_datagrams[i].data[0] )
                    {
                        break;
                    }
                    ++advertisedWnd;
                #ifdef OUTPUT_DATA_TO_CONSOLE
                    printf("Thread reads Seq #%d: %s\n", currentSeqNum, g_datagrams[i].data );
                #else
                    printf("Thread reads Seq #%d\n", currentSeqNum );
                #endif

                #ifdef OUTPUT_DATA_TO_FILE
                    fprintf( fp, "%s", g_datagrams[i].data );
                #endif
                    charCount += strlen( g_datagrams[i].data );
                    InvalidateClientDatagram( &g_datagrams[i] );
                }
                else
                {
                    // Current i points to an out-of-order datagram.
                    break;
                }
            }
        }
        LFR = ( i - 1 ) % datagramCount;
        //LAF = ( i + advertisedWnd - 1 ) % datagramCount;
        LAF = ( i + args.maxSlidingWindowSize - 1 ) % datagramCount;

        BOOL complete = g_AllAreAcked
                                && ( g_ack == ( currentSeqNum + 1 ) % datagramCount );
        //printf("Complete %d, ack %d, seq %d\n", complete, g_ack, currentSeqNum);
        //printf("cond1 %d, cond2 %d\n", g_AllAreAcked, g_ack == ( currentSeqNum + 1 ) % datagramCount);
        Pthread_mutex_unlock( &g_datagram_lock );

        if( TRUE == complete )
        {
            break;
        }

        // Let me sleep...
        double r =  (double)random() / MAX_RANDOM_NUMBER;
        if( r == 0.0 )
        {
            // the random number must be non-zero, because
            // log (0) is undefined.
            r = 0.0000000001;
        }
        unsigned int sleepInUS = (unsigned int) ( abs( 1000 * log( r ) ) ) * 1000;
        usleep( sleepInUS );
    }
#ifdef OUTPUT_DATA_TO_FILE
    fclose( fp );
#endif

    printf( "Total Character Count: %d\n", charCount );
    printf( "Thread exits\n" );
    pthread_exit(NULL);
}

// Helper function
static void
IncrementBufferIndex( int *index, int bufMaxSize )
{
    *index = *index + 1;
    *index = *index % bufMaxSize;
}

static void
DecrementBufferIndex( int *index, int bufMaxSize )
{
    if( 0 == *index )
    {
        *index = bufMaxSize - 1;
    }
    else
    {
        *index = *index - 1;
    }
}

/*static*/ void sigAlarmHandler( int signo )
{
    if( g_connecitonPhaseTimeout )
    {
        siglongjmp( jmpbuf, 1);
    }
}


