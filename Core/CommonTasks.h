#ifndef _COMMON_TASKS_H_
#define _COMMON_TASKS_H_
#include "unp.h"
#include "MyUnpRtt.h"

static const unsigned int MAX_NUMBER_OF_BUFFER = 512; // Max number of datagrams
static const unsigned int SERVER_DATAGRAM_TOTAL_SIZE = 512;
static const unsigned int CLIENT_DATAGRAM_TOTAL_SIZE = 512;
static const unsigned int SEQ_SIZE = 12;
static const unsigned int ACK_SIZE = 12;
static const unsigned int WND_SIZE = 8;
static const unsigned int TIME_SIZE = 24;
// THE FOLLOWING TWO VARIABLES SHOULD BE CONSISTENT
static const unsigned int SERVER_CLIENT_DATAGRAM_PAYOLAD_SIZE = 476;
#define SERVER_CLIENT_DATAGRAM_PAYOLAD_SIZE 476

#define WINDOW_PROBE_STRING "WinProbe"

typedef enum {FALSE = 0, TRUE} BOOL;

// Local means all local IP EXCEPT for the loopback address.
typedef enum mySrvCliRelation
{ NotLocal = 0, Local, LoopBack}
srvCliRelation;

typedef struct myNetworkInterfaceRecord
{
    int sockfd;
    char IPAddr[MAXLINE];
    char mask[MAXLINE];
    char subnetAddr[MAXLINE];
} networkInterfaceRecord;

typedef struct myClientDatagram
{
    unsigned int valid;
    unsigned int seqNum;
    //unsigned int awnd; // advertised window size
    uint32_t timestamp;
    char data[SERVER_CLIENT_DATAGRAM_PAYOLAD_SIZE];
} clientDatagram;


/**
    Fill in the IP address and network mask
    @param [out] record a pointer to a record's entry
    @param [out] ipStr a pointer to the IP adddress
    @param [out] ntmstr a pointer to the  network mask
    @param [in] IPAddr a pointer to sockaddr struct containing
                    IP info.
    @param [in] netmask a pointer to the  sockaddr struct
                    containing network mask info.
*/
void FillInIPAndNtm
    (
    networkInterfaceRecord *record,
    char *ipStr,
    char *ntmStr,
    struct sockaddr  *IPAddr,
    struct sockaddr  *netmask
    );

/**
    Fill in the subnet address
    @param [out] record a pointer to a record's entry
    @param [in] ipStr a pointer to the IP adddress
    @param [in] ntmstr a pointer to the  network mask
    @note the second and third params (ipStr and ntmStr)
        will be corrupted after this function call since we do
        GetToken on these params and change the pointer.
        Caller has to make sure that it does not use these
        params after calling this function.
*/
void FillInSubnetAddr
    (
    networkInterfaceRecord *record, // out
    char *ipStr,                    // input
    char *ntmStr                    // input
    );

void ParseServerDatagram
    (
    unsigned int *seqNum,
    uint32_t *timestamp,
    char *serverPayload,
    char *buffer
    );

void ParseClientDatagram
    (
    unsigned int *ackNum,
    unsigned int *advertisedWndSize,
    uint32_t *timestamp,
    char *buffer
    );

void BuildServerCommBuffer
    (
    char *buffer,
    const unsigned int seqNum,
    const uint32_t timestamp,
    char *serverPayload
    );

void BuildClientCommBuffer
    (
    char *buffer,
    const unsigned int ackNum,
    const unsigned int advertisedWndSize,
    const uint32_t timestamp
    );

void StartTimer( struct rtt_info *rttinfo );

void StopTimer( struct rtt_info *rttinfo, uint32_t timeStamp );

void StartPersistTimer();

void StopPersistTimer();

int
MySelect
    (
    int nfds,
    fd_set *readfds,
    fd_set *writefds,
    fd_set *exceptfds,
    struct timeval *timeout
    );


char *GetToken(char *str, char *token);


#endif
