#ifndef _ARGUMENTS_PARSER_
#define _ARGUMENTS_PARSER_

#include "unp.h"

typedef struct myServerArgrments
{
    unsigned int portNum;               //!< Well-known port number to which clients connect.
    unsigned int maxSlidingWindowSize;  //!< Sending window size in number of datagram
} serverArgs;

typedef struct myClientArgrments
{
    char IPAddr[MAXLINE];               //!< Server IP address
    unsigned int portNum;               //!< Server well-known port
    char requestFilename[MAXLINE];      //!< Filename to be transferred
    unsigned int maxSlidingWindowSize;  //!< Receiving window size in number of datagram
    unsigned int seed;                  //!< RNG's seed
     /**
        Probability of datagram loss, ranging [0.0, 1]
        Note that C does not support unsigned double
       */
    double dataLossProb;
     /**
        The mean time, in milliseconds, for an exponential distribution
        controlling the rate at which the client reads received datagram
        payloads from its receive buffer.
       */
    unsigned int meanInMS;
} clientArgs;

void ReadServerData( char filename[], serverArgs *data );

void ReadClientData( char filename[], clientArgs *data );

void DebugServerData( serverArgs data );

void DebugClientData( clientArgs data );

#endif
