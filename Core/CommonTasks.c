#include "CommonTasks.h"
#include "unp.h"
#include <sys/time.h>
#include "MyUnpRtt.h"

#ifdef DEBUG_COMMON_TASKS
#undef DEBUG_COMMON_TASKS
#endif

#ifdef DEBUG_TIMER
#undef DEBUG_TIMER
#endif

// Debug detailed client info
// Uncomment it if you want to debug
//#define DEBUG_COMMON_TASKS
#define DEBUG_TIMER

static const int MsPerSec = 1000;
static const int UsPerMs = 1000;

static const int PERSIST_TIMER_TIMEOUT_IN_US = 200000;

void FillInIPAndNtm
    (
    networkInterfaceRecord *record,
    char *ipStr,
    char *ntmStr,
    struct sockaddr  *IPAddr,
    struct sockaddr  *netmask
    )
{
    bzero( ipStr, MAXLINE );
    bzero( ntmStr, MAXLINE );

    if( AF_INET == IPAddr->sa_family )
    {
        struct sockaddr_in *saIP = (struct sockaddr_in *) IPAddr;
        Inet_ntop(AF_INET, &(saIP->sin_addr), ipStr, MAXLINE );
        struct sockaddr_in *saNtm = (struct sockaddr_in *) netmask;
        Inet_ntop(AF_INET, &(saNtm->sin_addr), ntmStr, MAXLINE );
    }
    else if( AF_INET6 == IPAddr->sa_family )
    {
        struct sockaddr_in6 *saIP = (struct sockaddr_in6 *) IPAddr;
        Inet_ntop(AF_INET6, &(saIP->sin6_addr), ipStr, MAXLINE );
        struct sockaddr_in6 *saNtm = (struct sockaddr_in6 *) netmask;
        Inet_ntop(AF_INET6, &(saNtm->sin6_addr), ntmStr, MAXLINE );
    }
    else
    {
        printf( "Unknown internet family.\n" );
        exit( 0 );
    }

    bcopy( ipStr, record->IPAddr, strlen( ipStr ) );
    bcopy( ntmStr, record->mask, strlen( ntmStr ) );
}

void FillInSubnetAddr
    (
    networkInterfaceRecord *record,
    char *ipStr,
    char *ntmStr
    )
{
    char *pIp = ipStr;
    char *pNtm = ntmStr;
    char IPSeg[MAXLINE];
    char NtmSeg[MAXLINE];
    char subnetAddr[MAXLINE];
    bzero( IPSeg, MAXLINE );
    bzero( NtmSeg, MAXLINE );
    bzero( subnetAddr, MAXLINE );
    while( 1 )
    {
        // Note that we will change the contents pointed by pIp and pNtm
        // But that is ok because we have copy the contents to another
        // buffers (records) before.
        pIp = GetToken( pIp, IPSeg );
        pNtm = GetToken( pNtm, NtmSeg );
        unsigned int subnetAddrSeg = atoi(IPSeg) & atoi(NtmSeg);
        char subnetAddrSegStr[MAXLINE];
        bzero( subnetAddrSegStr, MAXLINE );
        sprintf( subnetAddrSegStr, "%u", subnetAddrSeg );
        strcat( subnetAddr, subnetAddrSegStr );
        strcat( subnetAddr, "." );

        if( '\0' == pIp[0] || '\0' == pNtm[0] )
        {
            break;
        }
    }
    subnetAddr[strlen(subnetAddr)-1] = '\0'; // Discard the last '.'
    bcopy( subnetAddr, record->subnetAddr, strlen( subnetAddr ) );
}

void ParseServerDatagram
    (
    unsigned int *seqNum,
    uint32_t *timestamp,
    char *serverPayload,
    char *buffer
    )
{
    char data[SERVER_DATAGRAM_TOTAL_SIZE];

    // Get seqence number
    bzero( &data, SERVER_DATAGRAM_TOTAL_SIZE );
    bcopy( buffer, &data, SEQ_SIZE );
    *seqNum = atoi( data );

    // Get timestamp
    bzero( &data, SERVER_DATAGRAM_TOTAL_SIZE );
    bcopy( buffer + SEQ_SIZE, &data, TIME_SIZE );
    *timestamp = atoi( data );

    // Get payload
    // Note the last character is '\0'
    bzero( serverPayload, SERVER_CLIENT_DATAGRAM_PAYOLAD_SIZE );
    bcopy( &(*buffer) + SEQ_SIZE + TIME_SIZE, serverPayload, SERVER_CLIENT_DATAGRAM_PAYOLAD_SIZE );
}

void ParseClientDatagram
    (
    unsigned int *ackNum,
    unsigned int *advertisedWndSize,
    uint32_t *timestamp,
    char *buffer
    )
{
    char data[CLIENT_DATAGRAM_TOTAL_SIZE];

    // Get ack
    bzero( &data, CLIENT_DATAGRAM_TOTAL_SIZE );
    bcopy( buffer, &data, ACK_SIZE );
    *ackNum = atoi( data );

    // Get advertised window size
    bzero( &data, CLIENT_DATAGRAM_TOTAL_SIZE );
    bcopy( buffer + ACK_SIZE, &data, WND_SIZE );
    *advertisedWndSize = atoi( data );

    // Get timestamp
    bzero( &data, SERVER_DATAGRAM_TOTAL_SIZE );
    bcopy( buffer + ACK_SIZE + WND_SIZE, &data, TIME_SIZE );
    *timestamp = atoi( data );
}

void BuildServerCommBuffer
    (
    char *buffer,
    const unsigned int seqNum,
    const uint32_t timestamp,
    char *serverPayload
    )
{
    char data[SERVER_DATAGRAM_TOTAL_SIZE];

    // First, clean the output buffer
    bzero( buffer, SERVER_DATAGRAM_TOTAL_SIZE );

    // Copy seqence number
    bzero( &data, SERVER_DATAGRAM_TOTAL_SIZE );
    sprintf( data, "%u", seqNum );
    bcopy( &data, buffer, SEQ_SIZE );


    // Copy timestamp
    bzero( &data, SERVER_DATAGRAM_TOTAL_SIZE );
    sprintf( data, "%u", timestamp );
    bcopy( &data, buffer + SEQ_SIZE, TIME_SIZE );

    // Copy payload
    bzero( &data, SERVER_DATAGRAM_TOTAL_SIZE );
    if( serverPayload[0] != '\0')
    {
        bcopy( serverPayload, buffer + SEQ_SIZE + TIME_SIZE, SERVER_CLIENT_DATAGRAM_PAYOLAD_SIZE );
    }

    // Don't pack 'status'. It should not be sent.
}

void BuildClientCommBuffer
    (
    char *buffer,
    const unsigned int ackNum,
    const unsigned int advertisedWndSize,
    const uint32_t timestamp
    )
{
    char data[CLIENT_DATAGRAM_TOTAL_SIZE];

    // First, clean the output buffer
    bzero( buffer, CLIENT_DATAGRAM_TOTAL_SIZE );

    // Copy ack number
    bzero( &data, CLIENT_DATAGRAM_TOTAL_SIZE );
    sprintf( data, "%d", ackNum );
    bcopy( &data, buffer, ACK_SIZE );

    // Copy advertised window size
    bzero( &data, CLIENT_DATAGRAM_TOTAL_SIZE );
    sprintf( data, "%d", advertisedWndSize );
    bcopy( &data, buffer + ACK_SIZE, WND_SIZE );

    // Copy timestamp
    bzero( &data, CLIENT_DATAGRAM_TOTAL_SIZE );
    sprintf( data, "%d", timestamp );
    bcopy( &data, buffer + ACK_SIZE + WND_SIZE, TIME_SIZE );

    // Don't pack 'valid' and data. They should not be sent.
}

void StartTimer
    (
    struct rtt_info *rttinfo
    )
{
    struct itimerval tv;
    int currentRTOInMs = rtt_start( rttinfo );
    // Note the exact expired time is calculated with the combined
    // use of seconds and milliseconds.
    // Example: second = 3, millisecond = 145
    //     then the expired time is 3145 milliseconds.
    int expiredInSec = currentRTOInMs / MsPerSec;
    int expiredInUs = ( currentRTOInMs % MsPerSec ) * UsPerMs ;

    //printf("In StartTimer: %d (%d %d)\n", currentRTOInMs, expiredInSec, expiredInUs);

    // Setting interval to 0 disables the timer once the timer is expired.
    tv.it_interval.tv_sec = 0;
    tv.it_interval.tv_usec = 0;
    tv.it_value.tv_sec = expiredInSec;
    tv.it_value.tv_usec = expiredInUs;
    setitimer( ITIMER_REAL, &tv,0 );
#ifdef DEBUG_TIMER
    time_t ticks= time(NULL);
    // The output string of time information is 24. You can count it .
    // This exercise trains your eye balls :)
    printf( "Current time: %.24s. Set time out (ms):%d\n", ctime(&ticks), (tv.it_value.tv_sec)*1000+tv.it_value.tv_usec/1000 );
#endif
}

void StopTimer
    (
    struct rtt_info *rttinfo,
    uint32_t timeStamp
    )
{
    struct itimerval tv;
    // Configuration that disables the timer
    tv.it_value.tv_sec = 0;
    tv.it_value.tv_usec = 0;

    // call setitimer to disable the timer with the above configuration
    setitimer(ITIMER_REAL, &tv, 0);

    //printf( "===Stop timer: %d, %d, %d\n", rttinfo->rtt_rto, rtt_ts(rttinfo), timeStamp );
    rtt_stop( rttinfo, rtt_ts(rttinfo) - timeStamp );
}

void StartPersistTimer()
{
    struct itimerval tv;
    // Setting interval to 0 disables the timer once the timer is expired.
    tv.it_interval.tv_sec = 0;
    tv.it_interval.tv_usec = 0;
    tv.it_value.tv_sec = 1;
    tv.it_value.tv_usec = 0;
    if( setitimer( ITIMER_REAL, &tv, 0 ) < 0 )
    {
        err_sys("setitimer error");
    }
#ifdef DEBUG_TIMER
    time_t ticks= time( NULL );
    printf( "Start Persist Timer (timeout:%dms). Current time: %.24s\n", (tv.it_value.tv_sec)*1000+tv.it_value.tv_usec/1000, ctime(&ticks) );
#endif
}

void StopPersistTimer()
{
    struct itimerval tv;
    // Configuration that disables the timer
    tv.it_value.tv_sec = 0;
    tv.it_value.tv_usec = 0;

    // call setitimer to disable the timer with the above configuration
    setitimer(ITIMER_REAL, &tv, 0);
#ifdef DEBUG_TIMER
    time_t ticks= time( NULL );
    printf( "Stop Persist Timer. Current time: %.24s.\n", ctime(&ticks) );
#endif
}

int
MySelect
    (
    int nfds,
    fd_set *readfds,
    fd_set *writefds,
    fd_set *exceptfds,
    struct timeval *timeout
    )
{
    int     n;
    if ( (n = select(nfds, readfds, writefds, exceptfds, timeout)) < 0)
    {
        if( EINTR == errno )
        {
            //err_msg( "Non-fatal select error\n" );
        }
        else
        {
            err_quit( "select error" );
        }
    }
    return(n);      /* can return 0 on timeout */
}

//=============================================================================
char *
GetToken(char *str, char *token)
{
   int idx = 0;

   while(*str && (*str != '.') )
      token[idx++] = *str++;

   token[idx] = '\0';

   while(*str &&(*str == '.' || *str == '\t' || *str == '\n') )
      *str++;

   return str;
}


