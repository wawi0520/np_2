/**
   @file    UdpServ.c
   @author  Wei-Ying Tsai
   @contact wetsai@cs.stonybrook.edu
   @date 2015.10.29
*/

#include "unp.h"
#include "strings.h"
#include "ArgumentsParser.h"
#include "ServerTasks.h"

// Note that if we change this variable, we have to
// change the corresponding variable in ServerTasks.c
#define MAX_CONNECTION_RECORD 10

static const int MAX_CONNECTION = 5;
static const unsigned int MAX_SOCK_NUM = 32;
static const unsigned int MAX_NETWORK_REC = 64;

// This arrays record the the address and the port of clients
// that is currently connected.
static connectionRecord connRecords[MAX_CONNECTION_RECORD];
static unsigned int currentMaxNumOfConnRec;

void SigChldHandler( int signo);

//=============================================================================
int
main( int argc, char **argv )
{
    if( argc != 2 )
    {
        err_quit( "Usage: server server.in" );
    }

    serverArgs args;
    bzero( &args, sizeof( args ) );
    ReadServerData( argv[1], &args );
    DebugServerData( args );

    networkInterfaceRecord records[MAX_NETWORK_REC];
    unsigned int numOfNetworkRecords = Init( &records[0], MAX_NETWORK_REC, args.portNum );
    DebugServerRecords( records, numOfNetworkRecords );

    unsigned int i;
    int maxfd = 0;
    fd_set allset;
    fd_set rset;
    FD_ZERO( &allset );
    FD_ZERO( &rset );
    for( i = 0; i < numOfNetworkRecords; ++i )
    {
        int sockfd = records[i].sockfd;
        FD_SET( sockfd, &allset );
        if( sockfd > maxfd )
        {
            maxfd = sockfd;
        }
    }

    struct sockaddr_in cliaddr;
    socklen_t cliaddrLen = 0;
    bzero( &cliaddr, sizeof( cliaddr ) );

    // Initialize global variables
    currentMaxNumOfConnRec = 0;
    for( i = 0; i < MAX_CONNECTION_RECORD; ++i )
    {
        connRecords[i].pid = UNUSED_PID;
        bzero( &connRecords[i].clientAddrPort, MAXLINE );
    }

    while( 1 )
    {
        rset = allset;
        int connFd = 0;
        if( select( maxfd + 1, &rset, NULL, NULL, NULL ) < 0 )
        {
            if( EINTR == errno )
            {
                continue;
            }
            else
            {
                err_sys( "select error" );
            }
        }

        unsigned int i;
        for( i = 0; i < numOfNetworkRecords; ++i )
        {
            int sockfd = records[i].sockfd;
            if( FD_ISSET( records[i].sockfd, &rset ) )
            {
                if( MAX_CONNECTION_RECORD == currentMaxNumOfConnRec )
                {
                    printf( "No more connection record can be stored. Try to increase MAX_CONNECTION_RECORD defined in UdpServ.c\n" );
                    exit( 1 );
                }

                char recvMsg[MAXLINE];
                pid_t pid;
                cliaddrLen = sizeof( cliaddr );
                int n = Recvfrom( sockfd, recvMsg, MAXLINE, 0, (SA*) &cliaddr, &cliaddrLen );
                const unsigned int isConnected = IsAlreadyConnected( &connRecords[0], currentMaxNumOfConnRec, (SA*) &cliaddr );
                if( isConnected )
                {
                    // This client has been connected.
                    // Then, why does this client send a request again?
                    // Because previous data is lost, client retransmit it.
                    continue;
                }


                // Avoid zombie child processes.
                void SigChldHandler(int);
                Signal( SIGCHLD, SigChldHandler );
                if( 0 == ( pid = Fork() ) )
                {
                    char filename[MAXLINE];
                    bzero( &filename, MAXLINE );
                    bcopy( recvMsg, filename, MAXLINE );
                    printf( "Message received from %s: %s\n",
                        Sock_ntop( (SA *) &cliaddr, sizeof( cliaddr ) ), recvMsg );
                    int connSockfd = EstablishFileTransferConnection
                        (
                        &records[i],
                        (SA *) &cliaddr
                        );

                    printf( "Now start to transfer file %s...\n", recvMsg );
                    TransferFile( connSockfd, recvMsg, args );
                    exit( 0 );
                }
                else
                {
                    AddIntoConnectionRecords( &connRecords[0], &currentMaxNumOfConnRec, pid, (SA*) &cliaddr );
                }
            }
        }
    }
}

//=============================================================================
void SigChldHandler( int signo )
{
    pid_t pid;
    int stat;

    pid = wait( &stat );

    unsigned int i = 0;
    for( i = 0; i < currentMaxNumOfConnRec; ++i )
    {
        if( pid == connRecords[i].pid )
        {
            connRecords[i].pid = UNUSED_PID;
            bcopy(
                connRecords[currentMaxNumOfConnRec].clientAddrPort,
                connRecords[i].clientAddrPort,
                MAXLINE );
            --currentMaxNumOfConnRec;
            break;
        }
    }

    // Calling stardard IO functions in a signal handler is not
    // recommended. See textbook section 11.18.
    // Uncomment this only for diagnostic purpose.
    printf( "Child %d terminated.\n", pid );
    return;
}

