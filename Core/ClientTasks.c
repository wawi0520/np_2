#include "ClientTasks.h"
#include <time.h>
#include "unp.h"
#include "unpifiplus.h"
#include "MyUnpRtt.h"

#ifdef DEBUG_CLIENT_TASKS
#undef DEBUG_CLIENT_TASKS
#endif

// Debug detailed client info
// Uncomment it if you want to debug
//#define DEBUG_CLIENT_TASKS


static const char *loopbackAddr = "127.0.0.1";

unsigned int Init
(
    networkInterfaceRecord *records,
    const unsigned int maxRecords,
    clientArgs args
    )
{
    struct ifi_info *ifi;
    unsigned int recordsIndex = 0;
    for( ifi = Get_ifi_info_plus( AF_INET, 1 );
             ifi != NULL;
             ifi = ifi->ifi_next, ++recordsIndex )
    {
        if( recordsIndex == maxRecords )
        {
            printf( "The number of records found exceeds the maximum number allowed.\n" );
            printf( "Please go to UdpCli.c to change the MAX_NETWORK_REC.\n" );
            exit( 1 );
        }

        FillClientRecords( records, recordsIndex, ifi );
    }

    return recordsIndex;
}

void FillClientRecords
    (
    networkInterfaceRecord *records,
    const unsigned int recordsIndex,
    struct ifi_info *ifi
    )
{
    // Fill in sockfd
    // Currently unused, so give it -1.
    records[recordsIndex].sockfd = -1;

    // Fill in IP address and network mask
    struct sockaddr *IPAddr = ifi->ifi_addr;
    struct sockaddr *netmask = ifi->ifi_ntmaddr;
    char ipStr[MAXLINE];
    char ntmStr[MAXLINE];

    FillInIPAndNtm( &records[recordsIndex], ipStr, ntmStr, IPAddr, netmask );

    // Fill in subnet address
    FillInSubnetAddr( &records[recordsIndex], ipStr, ntmStr );
}

void DebugClientRecords
    (
    networkInterfaceRecord *records,
    const unsigned int numOfNetworkRecords
    )
{
    unsigned int i;
    printf( "=======Network Interface Summary=======\n" );
    for( i = 0; i < numOfNetworkRecords; ++i )
    {
        printf( "Record %u:\n", i + 1 );
        printf( "\tThe IP addr: %s.\n", records[i].IPAddr );
        printf( "\tThe network mask addr: %s.\n", records[i].mask );
        printf( "\tThe subnet addr: %s.\n", records[i].subnetAddr );
    }
    printf( "==================END==================\n" );
}

srvCliRelation InitIPServerAndIPClient
    (
    networkInterfaceRecord *records,
    const unsigned int numOfRecords,
    char *IPClient,
    char *IPServer,
    char *givenServerIPAddr // Server IP address that is read from client.in
    )
{
    // Ensure buffers are empty
    bzero( IPClient, MAXLINE );
    bzero( IPServer, MAXLINE );

    // Initially assigned the IP address given in client.in to our IPServer
    bcopy( givenServerIPAddr, IPServer, MAXLINE );
    unsigned int i = 0;
    unsigned int IPClientCandidate = 0;

    srvCliRelation relation = NotLocal;
    unsigned int recordIndex = 0;
    DetermineServClientRelation
        (
        &relation,
        &recordIndex,
        givenServerIPAddr,
        records,
        numOfRecords
        );


    // Set IPClient and IPServer
    // Also print the information to stdout
    printf( "===Relation between Server and Client===\n" );
    if( LoopBack == relation )
    {
        bcopy( loopbackAddr, IPClient, strlen(loopbackAddr) );
        IPClient[strlen(loopbackAddr)] = '\0';
        bcopy( loopbackAddr, IPServer, strlen(loopbackAddr) );
        IPServer[strlen(loopbackAddr)] = '\0';
        printf("The server is local\n" );
    }
    else if( Local == relation )
    {
        bcopy( records[recordIndex].IPAddr, IPClient, MAXLINE );
        printf("The server is local\n" );
    }
    else if( NotLocal == relation )
    {
        bcopy( records[recordIndex].IPAddr, IPClient, MAXLINE );
        printf("The server is not local\n" );
    }
    printf("IPServer: %s\n", IPServer );
    printf("IPClient: %s\n", IPClient );
    printf( "==================End===================\n" );

    return relation;
}

void DetermineServClientRelation
    (
    srvCliRelation *relation,
    unsigned int *recordIndex,
    char *givenIPAddr,
    networkInterfaceRecord *records,
    const unsigned int numOfRecords
    )
{
    *relation = NotLocal;    // By default, not local: 0
    unsigned int i = 0;

    // First, determine if loopback (if server ip = client ip)
    int relationIsDetermined = 0;
    for( i = 0; !relationIsDetermined && i < numOfRecords; ++i )
    {
        if( 0 == bcmp( givenIPAddr, records[i].IPAddr, MAXLINE ) )
        {
            // Both IPServer and IPClient are loopback address.
            *relation = LoopBack;
            relationIsDetermined = 1;
            break; // Loopback has the highest priority. We are done.
        }
    }

    // Second, let determine if local or not local.
    for( i = 0; !relationIsDetermined && i < numOfRecords; ++i )
    {
        char copyOfAClientRecord[MAXLINE];
        char copyOfGivenServerIPAddr[MAXLINE];
        char copyOfMask[MAXLINE];
        bzero( copyOfAClientRecord, MAXLINE );
        bzero( copyOfGivenServerIPAddr, MAXLINE );
        bzero( copyOfMask, MAXLINE );
        bcopy( records[i].IPAddr, copyOfAClientRecord, MAXLINE );
        bcopy( givenIPAddr, copyOfGivenServerIPAddr, MAXLINE );
        bcopy( records[i].mask, copyOfMask, MAXLINE );

        char *pStr1 = copyOfAClientRecord;
        char *pStr2 = copyOfGivenServerIPAddr;
        char *pStrMask = copyOfMask;
        char token1[MAXLINE];
        char token2[MAXLINE];
        char tokenMask[MAXLINE];
    #ifdef DEBUG_CLIENT_TASKS
        printf( "Client:%s, Server:%s, Mask:%d\n", pStr1, pStr2, pStrMask );
    #endif
        // Determine if the server is local
        while( 1 )
        {
            pStr1 = GetToken( pStr1, token1 );
            pStr2 = GetToken( pStr2, token2 );
            pStrMask = GetToken( pStrMask, tokenMask );

            const int subnetIDSeg1 = atoi(token1) & atoi(tokenMask);
            const int subnetIDSeg2 = atoi(token2) & atoi(tokenMask);
        #ifdef DEBUG_CLIENT_TASKS
            printf("Mask: %s, Client:%s, Server:%s, ClientSubID:%d, ServerSubID:%d.\n", tokenMask, token1, token2, subnetIDSeg1, subnetIDSeg2);
        #endif
            if( subnetIDSeg1 != subnetIDSeg2 )
            {
                *relation = NotLocal;
                *recordIndex = i;
                break;    // We are sure that client and server are not local.
            }
            else
            {
                *relation = Local; // "May be" local, so we still have to check.
                *recordIndex = i;
            }

            if( '\0' == pStr1[0] || '\0' == pStr2[0] || '\0' == pStrMask[0] )
            {
                // We have check all IP segment
                // We define IP segment which is splited by the '.' delimiter.
                break;
            }
        }

        if( Local == *relation )
        {
            // We find the local address. Exit the loop
            break;
        }
    }
}

unsigned int
FindAccumulativeAck
    (
    clientDatagram *datagrams,
    const unsigned int LFR,
    const unsigned int LAF,
    const unsigned int numOfDatagrams
    )
{
    //printf("LFR %u, LAF %u, count %d\n", LFR, LAF, numOfDatagrams );
    unsigned int largestSeqNum = ( LFR + 1 ) % numOfDatagrams;
    if( LFR == ( numOfDatagrams - 1 ) || LFR < LAF )
    {
        unsigned int i = ( LFR + 1 ) % numOfDatagrams ;
        for( ; i <= LAF; ++i )
        {
            if( FALSE == datagrams[i].valid )
            {
                break;
            }
        }
        largestSeqNum = i;
    }
    else
    {
        unsigned int i = ( LFR + 1 ) % numOfDatagrams;
        for( ; i < numOfDatagrams; ++i )
        {
            //printf("(FindAccumulativeAck)valid:%d\n",datagrams[i].valid);
            if( FALSE == datagrams[i].valid )
            {
                break;
            }
        }

        if( numOfDatagrams != i )
        {
            largestSeqNum = i;
        }
        else
        {
            for( i = 0; i <= LAF; ++i )
            {
                if( FALSE == datagrams[i].valid )
                {
                    break;
                }
            }
            largestSeqNum = i;
        }
    }
    //printf("(FindAccumulativeAck)largestSeqNum:%d\n",largestSeqNum);

    return largestSeqNum;
}

BOOL ClientDatagramExists
    (
    clientDatagram *datagrams,
    const unsigned int seq
    )
{
    return datagrams[seq].valid;
}

void SetClientDatagram
    (
    clientDatagram *datagram,
    const BOOL valid,
    const uint32_t timestamp,
    char *payload
    )
{
    datagram->valid = valid;
    datagram->timestamp = timestamp;
    bzero( &datagram->data, SERVER_CLIENT_DATAGRAM_PAYOLAD_SIZE );
    bcopy( payload, &datagram->data, SERVER_CLIENT_DATAGRAM_PAYOLAD_SIZE );
}

BOOL MyWrite
    (
    int sockfd,
    char *sendBuf,
    const unsigned int size,
    const double givenProb
    )
{
    double prob = ( (double)random() ) / MAX_RANDOM_NUMBER;
    if( prob < givenProb )
    {
        printf("Decide not to send this datagram\n");
        return FALSE;
    }
    Write( sockfd, sendBuf, size );

    return TRUE;
}

void InvalidateClientDatagram( clientDatagram *datagrams )
{
    datagrams->valid = FALSE;
}

