/* include rtt1 */
#include    "MyUnpRtt.h"

int        rtt_d_flag = 0;        /* debug flag; can be set by caller */

/*
 * Calculate the RTO value based on current estimators:
 *        smoothed RTT plus four times the deviation
 */
#define    RTT_RTOCALC(ptr) ((ptr)->rtt_srtt + (4 * (ptr)->rtt_rttvar))

static int
rtt_minmax(int rto)
{
    if (rto < RTT_RXTMIN)
    {
        rto = RTT_RXTMIN;
    }
    else if (rto > RTT_RXTMAX)
    {
        rto = RTT_RXTMAX;
    }
    return(rto);
}

void
rtt_init(struct rtt_info *ptr)
{
    struct timeval    tv;

    Gettimeofday(&tv, NULL);
    ptr->rtt_base = tv.tv_sec ;        /* # sec since 1/1/1970 at start */

    ptr->rtt_rtt    = 0;
    ptr->rtt_srtt   = 0;
    ptr->rtt_rttvar = 250;
    ptr->rtt_rto = rtt_minmax(RTT_RTOCALC(ptr));
        /* first RTO at (srtt + (4 * rttvar)) = 3 seconds */
    //ptr->rtt_srtt = ptr->rtt_srtt * 8;
    //ptr->rtt_rttvar = ptr->rtt_rttvar * 4;
}
/* end rtt1 */

/*
 * Return the current timestamp.
 * Our timestamps are 32-bit integers that count milliseconds since
 * rtt_init() was called.
 */

/* include rtt_ts */
uint32_t
rtt_ts(struct rtt_info *ptr)
{
    uint32_t        ts;
    struct timeval    tv;

    Gettimeofday(&tv, NULL);
    ts = ((tv.tv_sec - ptr->rtt_base) * 1000) + (tv.tv_usec / 1000);
    return(ts);
}

void
rtt_newpack(struct rtt_info *ptr)
{
    ptr->rtt_nrexmt = 0;
}

int
rtt_start(struct rtt_info *ptr)
{
    return((ptr->rtt_rto));
        /* 4return value can be used as: alarm(rtt_start(&foo)) */
}
/* end rtt_ts */

/*
 * A response was received.
 * Stop the timer and update the appropriate values in the structure
 * based on this packet's RTT.  We calculate the RTT, then update the
 * estimators of the RTT and its mean deviation.
 * This function should be called right after turning off the
 * timer with alarm(0), or right after a timeout occurs.
 */

/* include rtt_stop */
void
rtt_stop(struct rtt_info *ptr, uint32_t ms )
{
    if( 0 == ptr->rtt_srtt )
    {
        ptr->rtt_srtt = (int) ms << 3;
        // The rttvar should be of a scaled-up form, at 4 times its
        // real value. Plus, the initial rttvar should be equal to 0.5
        // times the first measured rtt. So here, we only left shift
        // by 1, which is equal to 2 times ( = 4 x 0.5).
        ptr->rtt_rttvar = (int) ms << 1;
    }
    //printf("ms : %d\n", ms );
    //printf("ptr->rtt_srtt: %d\n", ptr->rtt_srtt);
    //printf("rtt_rttvar: %d\n", ptr->rtt_rttvar);
    ptr->rtt_rtt = ms;
    int measuredRttInMs;
    measuredRttInMs = (int)(ms) - (ptr->rtt_srtt >> 3);
    ptr->rtt_srtt += measuredRttInMs;
    if( measuredRttInMs < 0 )
    {
         measuredRttInMs = -1 * measuredRttInMs;
    }
    measuredRttInMs -= (ptr->rtt_rttvar >> 2);
    ptr->rtt_rttvar += measuredRttInMs;
    ptr->rtt_rto = (ptr->rtt_srtt >> 3) + ptr->rtt_rttvar;
    //printf("Stop: rto:%d\n", ptr->rtt_rto);
    ptr->rtt_rto = rtt_minmax(ptr->rtt_rto);
    // Make sure that the rto is always no less than 1 millisecond.
    ptr->rtt_rto = max( ptr->rtt_rto, 1 );
    printf("Stop timer: rtt (%u), rto (%d), srtt (%d), rttvar (%d)\n", ms, ptr->rtt_rto, ptr->rtt_srtt>>3, ptr->rtt_rttvar>>2 );
}
/* end rtt_stop */

/*
 * A timeout has occurred.
 * Return -1 if it's time to give up, else return 0.
 */

/* include rtt_timeout */
int
rtt_timeout(struct rtt_info *ptr)
{
    ptr->rtt_rto *= 2;        /* next RTO */
    ptr->rtt_rto = rtt_minmax(ptr->rtt_rto);

    if (++ptr->rtt_nrexmt > RTT_MAXNREXMT)
        return(-1);            /* time to give up for this packet */
    return(0);
}
/* end rtt_timeout */

/*
 * Print debugging information on stderr, if the "rtt_d_flag" is nonzero.
 */

void
rtt_debug(struct rtt_info *ptr)
{
    if (rtt_d_flag == 0)
        return;

    fprintf(stderr, "rtt = %.3f, srtt = %.3f, rttvar = %.3f, rto = %.3f\n",
            ptr->rtt_rtt, ptr->rtt_srtt, ptr->rtt_rttvar, ptr->rtt_rto);
    fflush(stderr);
}
