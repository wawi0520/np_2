#include "unp.h"
#include "ArgumentsParser.h"

#ifdef DEBUG_ARGUMENTS_PARSERS
#undef DEBUG_ARGUMENTS_PARSERS
#endif

// Comment out the macro if you want to debug this module
//#define DEBUG_ARGUMENTS_PARSERS

void ReadServerData( char filename[], serverArgs *data )
{
#ifdef DEBUG_ARGUMENTS_PARSERS
    printf( "=====Debug Info: ReadServerData=====\n" );
    printf( "The server reads in %s(%d) file.\n", filename, strlen(filename) );
    printf( "================END=================\n" );
#endif

    FILE *fp = Fopen( filename, "rb" );

    int portNum;
    fscanf( fp, "%u", &portNum );

    unsigned int windowSize;
    fscanf( fp, "%u", &windowSize );

    data->portNum = portNum;
    data->maxSlidingWindowSize = windowSize;

    fclose( fp );
}

void ReadClientData( char filename[], clientArgs *data )
{
#ifdef DEBUG_ARGUMENTS_PARSERS
    printf( "=====Debug Info: ReadClientData=====\n" );
    printf( "The client reads in %s(%d) file.\n", filename, strlen(filename) );
#endif

    FILE *fp = Fopen( filename, "rb" );

    char IPAddr[MAXLINE];
    fscanf( fp, "%s", &IPAddr );

    int portNum;
    fscanf( fp, "%u", &portNum );

    char requestFilename[MAXLINE];
    fscanf( fp, "%s", &requestFilename );

    unsigned int windowSize;
    fscanf( fp, "%u", &windowSize );

    unsigned int seed;
    fscanf( fp, "%u", &seed );

    double dataLossProb;
    fscanf( fp, "%lf", &dataLossProb );

    unsigned int meanInMS;
    fscanf( fp, "%u", &meanInMS );

    bcopy( IPAddr, data->IPAddr, strlen( IPAddr ) );
    data->portNum = portNum;
    bcopy( requestFilename, data->requestFilename, strlen( requestFilename ) );
    data->maxSlidingWindowSize = windowSize;
    data->seed = seed;
    data->dataLossProb = dataLossProb;
    data->meanInMS = meanInMS;

    fclose( fp );
}

void DebugServerData( serverArgs data )
{
#ifdef DEBUG_ARGUMENTS_PARSERS
    printf( "=====Debug Info: DebugServerData=====\n" );
    printf( "Server well-known port number: %u\n", data.portNum);
    printf( "Server Sending window size: %u\n", data.maxSlidingWindowSize );
    printf( "=================END=================\n" );
#endif
}

void DebugClientData( clientArgs data )
{
#ifdef DEBUG_ARGUMENTS_PARSERS
    printf( "=====Debug Info: DebugClientData=====\n" );
    printf( "Client connects to :%s(%d)\n", data.IPAddr, strlen( data.IPAddr ) );
    printf( "Client connects to server with port number: %u\n", data.portNum);
    printf( "Client requests %s(%d)\n", data.requestFilename, strlen( data.requestFilename ) );
    printf( "Client sending window size: %u\n", data.maxSlidingWindowSize );
    printf( "Client RNG with seed: %u\n", data.seed );
    printf( "Client simulated probability of datagram loss: %.3lf\n", data.dataLossProb );
    printf( "Client mean for an exponential dist. controlling the client reading rate: %u\n", data.meanInMS );
    printf( "=================END=================\n" );
#endif
}


