#ifndef _CLIENT_TASKS_H_
#define _CLIENT_TASKS_H_

#include "unp.h"
#include "ArgumentsParser.h"
#include "unpifiplus.h"
#include "CommonTasks.h"
static const unsigned int MAX_RANDOM_NUMBER = 2147483647; // (2^31) -1

unsigned int Init
    (
    networkInterfaceRecord *records,
    const unsigned int maxRecords,
    clientArgs args
    );

void FillClientRecords
    (
    networkInterfaceRecord *records,
    const unsigned int recordsIndex,
    struct ifi_info *ifi
    );

void DebugClientRecords
    (
    networkInterfaceRecord *records,
    const unsigned int numOfNetworkRecords
    );

srvCliRelation InitIPServerAndIPClient
    (
    networkInterfaceRecord *records,
    const unsigned int numOfRecords,
    char *IPClient,
    char *IPServer,
    char *givenServerIPAddr // Server IP address that is read from client.in
    );

/**
    Determining the relation between server and client
    @param [out]  relation the relation between server and client
    @param [out]  recordIndex the index to the selected record
    @param [in] givenIPAddr string which specifies the server
                        address given by *.in file
    @param [in] records a pointer to all network interface records
    @param [in] numOfRecords the number of entries in network
                        interface records.
*/
void DetermineServClientRelation
    (
    srvCliRelation *relation,
    unsigned int *recordIndex,
    char *givenIPAddr,
    networkInterfaceRecord *records,
    const unsigned int numOfRecords
    );

int EstablishFileTransferConnection
    (
    int sockfd,
    char *requestFilename,
    int *newServPort
    );

unsigned int FindAccumulativeAck
    (
    clientDatagram *datagrams,
    const unsigned int LFR,
    const unsigned int LAF,
    const unsigned int numOfDatagrams
    );

BOOL ClientDatagramExists
    (
    clientDatagram *datagrams,
    const unsigned int seq
    );

void SetClientDatagram
    (
    clientDatagram *datagram,
    const BOOL valid,
    const uint32_t timestamp,
    char *payload
    );

/**Wrapper function for Write
    Before writing, we generate a probability that determines
    whether to send the data or not.
*/
BOOL MyWrite
    (
    int sockfd,
    char *sendBuf,
    const unsigned int size,
    const double givenProb
    );

void InvalidateClientDatagram( clientDatagram *datagrams );

static void sigAlarmHandler( int signo );


#endif

