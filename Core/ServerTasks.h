#ifndef _SERVER_TASKS_H_
#define _SERVER_TASKS_H_

#include "unp.h"
#include "ArgumentsParser.h"
#include "CommonTasks.h"
#include "MyUnpRtt.h"

typedef struct myConnecitonRecord
{
    pid_t pid;
    char clientAddrPort[MAXLINE];
} connectionRecord;

// Sent means the datagram has been sent BUT NOT acked.
// Acked means the datagram has been sent AND acked.
typedef enum {Unused = 0, Sent, Acked, NeedRetransmitting} datagramStatus;

typedef enum myCongestionCtrlState
{ SlowStart = 0, CongestionAvoidance, FastRecovery }
congestionCtrlState;

// Specify the sliding window configuration
typedef struct mySlidingWindowConfig
{
    unsigned int bufferSize;
    int LAR;
    int LFS;
    //int notSent;
} slidingWindowConfig;

typedef struct myServerDatagram
{
    datagramStatus status;
    unsigned int seqNum;
    uint32_t timestamp;
    char data[SERVER_CLIENT_DATAGRAM_PAYOLAD_SIZE];
} serverDatagram;

// The maximum number of ackInfos is equal to
// the given sliding window size in server.in
typedef struct myAckInfo
{
    unsigned int seqNumThatAcks;
    uint32_t timestamp;
} ackInfo;

static const pid_t UNUSED_PID = -1;

unsigned int Init
        (
        networkInterfaceRecord *records,
        const unsigned int maxRecords,
        const unsigned int boundPortNum
        );

void FillServerRecords
    (
    networkInterfaceRecord *records,
    const unsigned int recordsIndex,
    int sockfd,
    struct sockaddr  *IPAddr,
    struct sockaddr  *netmask
    );

int EstablishFileTransferConnection
    (
    networkInterfaceRecord *records,
    SA *cliaddr
    );

const int IsAlreadyConnected
    (
    connectionRecord *connRecords,
    const unsigned int currentMaxNumOfConnRecords,
    SA *cliaddr
    );

void AddIntoConnectionRecords
    (
    connectionRecord *connRecords,
    unsigned int *currentMaxNumOfConnRecords,
    const pid_t pid,
    SA *cliaddr
    );

void TransferFile( int sockfd, char *filename, serverArgs args );

const unsigned int ReadClientResponse
    (
    ackInfo *ackInfos,
    unsigned int *ackInfoCount,
    unsigned int *advertisedWndSize,
    slidingWindowConfig *wndConfig,
    serverDatagram *datagrams,
    struct rtt_info *RTOTimerInfo,
    int sockfd,
    int pipeReadFd
    );

void SetServerDatagram
    (
    serverDatagram *datagram,
    unsigned int seqNum,
    uint32_t timestamp,
    char *payload
    );

void ResetServerDatagram( serverDatagram *datagram );

void InitSlidingWindowConfig
    (
    slidingWindowConfig *slidingWndConf,
    const unsigned int bufSize,
    const unsigned int wndSize
    );

int IsSeqNumInSlidingWindow
    (
    slidingWindowConfig * wndConfig,
    unsigned int seqNum
    );

void AddAckInfo
    (
    ackInfo *ackInfos,
    unsigned int *ackCount,
    const unsigned int seqNum,
    const uint32_t timestamp
    );

BOOL  SlideWindow
    (
    slidingWindowConfig *slidingWndConf,
    ackInfo *ackInfos,
    const unsigned int ackInfoCount
    );

unsigned int
FindLargestSeqNum
    (
    const unsigned int LAR,
    const unsigned int LFS,
    const unsigned int datagramCount,
    ackInfo *ackInfos,
    const unsigned int ackInfoCount
    );

const int SeqNumExistInAckInfo
    (
    const unsigned int key,
    ackInfo *ackInfos,
    const unsigned int ackInfoCount
    );

void CongestionControl
    (
    unsigned int *pCwnd,
    unsigned int *pssthresh,
    congestionCtrlState *pState,
    unsigned int *pDuplicateCount,
    unsigned int thisAck,
    int *pLastAckReceived,
    serverDatagram *datagram
    );

void DebugConnectionRecords
    (
    connectionRecord *connRecords,
    const unsigned int currentMaxNumOfConnRecords
    );


void DetermineServClientRelation
    (
    srvCliRelation *relation,
    char *servAddr,
    char *mask,
    char *subnetAddr,
    char *cliAddr
    );

void DebugServerRecords
    (
    networkInterfaceRecord *records,
    const unsigned int numOfNetworkRecords
    );

const unsigned int Min( const unsigned int a, const unsigned int b );
const unsigned int Max( const unsigned int a, const unsigned int b );
void SetMSGPeek( int *flag );
void ResetMSGPeek( int *flag );
void SetSocketNonBlocking( int *sockfd );
void SetSocketBlocking( int *sockfd );


void IncrementBufferIndex( unsigned int *index, int maxSize );


#endif
