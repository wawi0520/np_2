#include "ServerTasks.h"
#include <setjmp.h>
#include "unp.h"
#include "unpifiplus.h"


#ifdef DEBUG_SERVER_TASKS
#undef DEBUG_SERVER_TASKS
#endif

// Note that if we change this variable, we have to
// change the corresponding variable in UdpServ.c
#define MAX_CONNECTION_RECORD 10

// Debug detailed server network interface info
// Uncomment it if you want to debug
//#define DEBUG_SERVER_TASKS
//#define DEBUG_SERVER_TASKS_DETAILED

// Time-out
static void sigAlarmHandler( int signo );

// Pipe
#define PIPE_FD_COUNT 2
#define PIPE_READ_FD 0
#define PIPE_WRITE_FD 1
int g_pfd[PIPE_FD_COUNT]; // 0 is read; 1 is write

// Congestion Avoidance
static BOOL g_RTOTimerTimeout = FALSE;
static unsigned int g_cwnd;
static unsigned int g_ssthresh;
static congestionCtrlState g_state;
static unsigned int g_dupAckCount;
static unsigned int g_thisAck;
static int g_lastAckReceived;

//=============================================================================
unsigned int
Init
    (
    networkInterfaceRecord *records,
    const unsigned int maxRecords,
    const unsigned int boundPortNum
    )
{
    unsigned int tatalRecord = 0;

    int sockfd;
    const int on = 1;
    unsigned int recordsIndex = 0;
    struct ifi_info *ifi;
    struct sockaddr_in *sa, cliaddr, wildaddr;

    for( ifi = Get_ifi_info_plus( AF_INET, 1 );
         ifi != NULL;
         ifi = ifi->ifi_next, ++recordsIndex )
    {
        if( recordsIndex == maxRecords )
        {
            printf( "The number of records found exceeds the maximum number allowed.\n" );
            printf( "Please go to UdpServ.c to change the MAX_NETWORK_REC.\n" );
            exit( 1 );
        }

        sockfd = Socket( AF_INET, SOCK_DGRAM, 0 );

        sa = (struct sockaddr_in *) ifi->ifi_addr;
        sa->sin_family = AF_INET;
        sa->sin_port = htons( boundPortNum );
        /* bind unicast address */
        Bind( sockfd, (SA *) sa, sizeof( *sa ) );
        printf("bound %s\n", Sock_ntop((SA *) sa, sizeof(*sa)));

        // Fill in all data
        ++tatalRecord;
        FillServerRecords( records, recordsIndex, sockfd, ifi->ifi_addr, ifi->ifi_ntmaddr );
    }

    // Create a pipe for one-way communication
    if( -1 == pipe( g_pfd ) )
    {
        err_sys( "pipe failed.\n" );
    }

    Signal( SIGALRM, sigAlarmHandler );

    return tatalRecord;
}

//=============================================================================
void
FillServerRecords
    (
    networkInterfaceRecord *records,
    const unsigned int recordsIndex,
    int sockfd,
    struct sockaddr  *IPAddr,
    struct sockaddr  *netmask
    )
{
    // Fill in sockfd
    records[recordsIndex].sockfd = sockfd;

    // Fill in IP address and network mask
    char ipStr[MAXLINE];
    char ntmStr[MAXLINE];
    FillInIPAndNtm( &records[recordsIndex], ipStr, ntmStr, IPAddr, netmask );

    // Fill in subnet address
    FillInSubnetAddr( &records[recordsIndex], ipStr, ntmStr );
}

int EstablishFileTransferConnection
    (
    networkInterfaceRecord *records,
    SA *cliaddr
    )
{
    int listenSockfd = records->sockfd;
    socklen_t cliaddrLen = sizeof( *cliaddr );

    struct sockaddr_in servaddr;
    bzero( &servaddr, sizeof( servaddr ) );
    servaddr.sin_family = AF_INET;
    Inet_pton( AF_INET, records->IPAddr, &servaddr.sin_addr );
    servaddr.sin_port = htons( 0 ); // Kernel determines the port number
    int connectSockfd = Socket( AF_INET, SOCK_DGRAM, 0 );
    Bind( connectSockfd, (SA *) &servaddr, sizeof( servaddr ) );
    // Retrieve the client IP address and port number set by the kernel.
    socklen_t servaddrLen = sizeof(servaddr);
    if( getsockname( connectSockfd, (SA *)&servaddr, &servaddrLen ) < 0 )
    {
        err_ret("getsockname error");
    }
    printf("Child bound %s.\n", Sock_ntop((SA *) &servaddr, sizeof(servaddr)) );

    srvCliRelation relation = NotLocal;
    unsigned int recordIndex = 0;
    char givenClientIPAddr[MAXLINE];
    bzero( givenClientIPAddr, MAXLINE );
    Inet_ntop(AF_INET, &(((struct sockaddr_in *)cliaddr)->sin_addr), givenClientIPAddr, MAXLINE );
    DetermineServClientRelation
        (
        &relation,
        records->IPAddr,    // Server Address
        records->mask,// Network mask
        records->subnetAddr,// Subnet
        givenClientIPAddr   // Client Address
        );

    printf( "===Relation between Server and Client===\n" );
    if( Local == relation || LoopBack == relation )
    {
    #ifdef DEBUG_SERVER_TASKS
        int optVal = 0;
        int optValLen = 0;
        getsockopt( connectSockfd, SOL_SOCKET, SO_DONTROUTE, &optVal , &optValLen );
        printf( "SO_DONTROUTE is :%d\n", optVal );
    #endif
        int setValue = 1;
        setsockopt( connectSockfd, SOL_SOCKET, SO_DONTROUTE, &setValue, sizeof(setValue) );
    #ifdef DEBUG_SERVER_TASKS
        printf( "Local: set SO_DONTROUTE.\n" );
        getsockopt( connectSockfd, SOL_SOCKET, SO_DONTROUTE, &optVal , &optValLen );
        printf( "SO_DONTROUTE is :%d\n", optVal );
    #endif
        printf("The client is local\n" );
    }
    else
    {

        printf("The client is not local\n" );
    }
    printf( "==================End===================\n" );

    printf("Connect to: %s\n", Sock_ntop( cliaddr, cliaddrLen ) );
    Connect( connectSockfd, cliaddr, cliaddrLen );

    char sendline[MAXLINE];
    char recvline[MAXLINE];
    bzero( &sendline, MAXLINE );
    bzero( &recvline, MAXLINE );

    // Time out variables
    struct rtt_info rttInfo;
    rtt_init( &rttInfo );
    rtt_d_flag = 0; // don't show additional info
    rtt_newpack( &rttInfo );
    while( 1 )
    {
        sprintf( sendline, "Port:%d", servaddr.sin_port );
        printf( "Send connection port (%d) to the client.\n", servaddr.sin_port );
        Sendto( listenSockfd, sendline, strlen( sendline ), 0, cliaddr, cliaddrLen );

        // Start the timer
        StartTimer( &rttInfo );

        fd_set allrset;
        fd_set rset;
        FD_ZERO( &allrset );
        FD_ZERO( &rset );
        FD_SET( connectSockfd, &allrset );
        FD_SET( g_pfd[PIPE_READ_FD], &allrset );
        int maxfd = Max( connectSockfd, g_pfd[PIPE_READ_FD] );

        rset = allrset;
        int nReady = MySelect( maxfd+1, &rset, NULL, NULL, NULL );
        if( FD_ISSET( g_pfd[PIPE_READ_FD], &rset ) )
        {
            // Just read 1 byte from the pipe and we don't process it.
            char recvBuf[32];
            read( g_pfd[PIPE_READ_FD], recvBuf, sizeof(recvBuf) );

            // Time out. The server sends the client the new port again
            if( rtt_timeout( &rttInfo ) < 0 )
            {
                err_sys( "No response from client after sending new port number. Give up." );
                errno = ETIMEDOUT;
                return (-1);
            }

            char buf[MAXLINE];
            bzero( &buf, MAXLINE );
            time_t ticks = time( NULL );
            snprintf( buf, MAXLINE, "%s", ctime( &ticks ) );
            printf( "Current Time: %sSocket time out: no response after sending new port number.\n", buf );
            printf( "Re-send connection port (%d) to the client.\n", servaddr.sin_port );
            Write( connectSockfd, sendline, strlen( sendline ) );

            // Update timeout and start the timer again
            uint32_t currentTimestamp = rtt_ts(&rttInfo);
            //StopTimer( &rttInfo, currentTimestamp );
            StartTimer( &rttInfo );
        }

        if( FD_ISSET( connectSockfd, &rset ) )
        {
            int nRead = Recvfrom( connectSockfd, recvline, MAXLINE, 0, cliaddr, &cliaddrLen );
            recvline[ nRead ] = 0; // null-terminate
            if( nRead > 0 )
            {
                printf("Message received from %s: %s.\n", Sock_ntop( cliaddr, cliaddrLen ), recvline );

                if( 0 == bcmp( recvline, "ok", strlen( "ok" ) ) )
                {
                    printf( "Connection is ok.\n" );
                    break;
                }
                else
                {
                    //err_msg( "Un-recognized message. Continue..." );
                }
            }
        }
    }

    // If we are here, then the file transfer connection must
    // have been established.
    close( listenSockfd );
    StopTimer( &rttInfo, 0 );

    return connectSockfd;
}

const int IsAlreadyConnected
    (
    connectionRecord *connRecords,
    const unsigned int currentMaxNumOfConnRecords,
    SA *cliaddr
    )
{
    int isConnected = 0;
    unsigned int i = 0;
    char cliInfo[MAXLINE];
    bzero( &cliInfo, MAXLINE );
    for( i = 0; i < currentMaxNumOfConnRecords && !isConnected ; ++i )
    {
        if( UNUSED_PID == connRecords[i].pid )
        {
            // This entry is unused. We don't have to check it.
            continue;
        }
        sprintf( cliInfo, "%s", Sock_ntop( cliaddr, sizeof(*cliaddr) ) );
        if( 0 == bcmp( connRecords[i].clientAddrPort, cliInfo, MAXLINE ) )
        {
            // Already connected
            isConnected = 1;
            break;
        }
    }

    return isConnected;
}

void AddIntoConnectionRecords
    (
    connectionRecord *connRecords,
    unsigned int *currentMaxNumOfConnRecords,
    const pid_t pid,
    SA *cliaddr
    )
{
    char cliInfo[MAXLINE];
    bzero( &cliInfo, MAXLINE );
    sprintf( cliInfo, "%s", Sock_ntop( cliaddr, sizeof(*cliaddr) ) );

    unsigned int i = 0;
#ifdef DEBUG_SERVER_TASKS_DETAILED
    printf("Current %d entries.\n",*currentMaxNumOfConnRecords);
#endif
    if( UNUSED_PID == connRecords[*currentMaxNumOfConnRecords].pid )
    {
        connRecords[*currentMaxNumOfConnRecords].pid = pid;
        bcopy( cliInfo, connRecords[*currentMaxNumOfConnRecords].clientAddrPort, MAXLINE );
    }
    else
    {
    #ifdef DEBUG_SERVER_TASKS_DETAILED
        printf("Non-fatal error: The entry of connRecords is used. ");
        printf("Please check.\n");
    #endif
    }
    (*currentMaxNumOfConnRecords) += 1;
#ifdef DEBUG_SERVER_TASKS_DETAILED
    printf("Current %d entries.\n",*currentMaxNumOfConnRecords);
#endif
}

void TransferFile( int sockfd, char *filename, serverArgs args )
{
    FILE *fp = Fopen( filename, "rb" );

    char sendBuf[SERVER_DATAGRAM_TOTAL_SIZE];
    char payload[SERVER_CLIENT_DATAGRAM_PAYOLAD_SIZE];
    bzero( &sendBuf, SERVER_DATAGRAM_TOTAL_SIZE );
    int nRead = 0;
    int lastNRead = 0;
    unsigned int seqNum = 0;
    unsigned int lastSeqNum = 0;
    unsigned int datagramCount = MAX_NUMBER_OF_BUFFER;
    serverDatagram *datagrams;
    unsigned int sendingWndSize = args.maxSlidingWindowSize;
    struct rtt_info rttInfo;
    //struct rtt_info RTOTimerInfo;
    rtt_init( &rttInfo );
    //rtt_init( &RTOTimerInfo );
    uint32_t timestamp = 0;

    datagrams = (serverDatagram *)Calloc( datagramCount, sizeof( serverDatagram ) );
    // Initialize ssthresh using the maximum SENDER buffer size.

    unsigned int advertisedWndSize = 1;
    unsigned int determinedSendSize = 0;
    int isEOFRead = 0;

    // Congestion control
    g_cwnd = 1;  // congestion window
    g_ssthresh = MAX_NUMBER_OF_BUFFER;
    g_state = SlowStart;
    g_dupAckCount = 0;
    // Right. This is int, not unsigned int, because we have to
    // use -1 to indicate the initial condition
    g_lastAckReceived = -1;

    slidingWindowConfig slidingWndConf;
    InitSlidingWindowConfig( &slidingWndConf, datagramCount, args.maxSlidingWindowSize );
    unsigned int ackInfoCount = 0; // associated with the number of ackInfo
    ackInfo *ackInfos;
    ackInfos = (ackInfo *)Calloc( args.maxSlidingWindowSize, sizeof( ackInfo ) );

    while( 1 )
    {
        unsigned int sendFirstDatagramInWindow = 1;
        bzero( &sendBuf, SERVER_DATAGRAM_TOTAL_SIZE );
        bzero( &payload, SERVER_CLIENT_DATAGRAM_PAYOLAD_SIZE );
        determinedSendSize = Min( advertisedWndSize, sendingWndSize );
        determinedSendSize = Min( determinedSendSize, g_cwnd );
        // Not sure if window size < 1 wil happen.
        // Just to make sure we don't encounter wierd case
        /*determinedSendSize = ( determinedSendSize < 1 )?
                                1: determinedSendSize;*/

        // Update LFS
        slidingWndConf.LFS = ( slidingWndConf.LAR + args.maxSlidingWindowSize ) % datagramCount;
        seqNum = ( slidingWndConf.LAR + 1 ) % datagramCount;
        printf("File Transfer begins: %d, send size: %d, seq: %d.\n", seqNum, determinedSendSize, seqNum );

        unsigned int sentCount = 0;
        for( ; determinedSendSize > 0 ; --determinedSendSize, IncrementBufferIndex( &seqNum, MAX_NUMBER_OF_BUFFER) )
        {
            bzero( &sendBuf, SERVER_DATAGRAM_TOTAL_SIZE );
            bzero( &payload, SERVER_CLIENT_DATAGRAM_PAYOLAD_SIZE );

            //printf("status: %d\n", datagrams[seqNum].status );
            if( NeedRetransmitting== datagrams[seqNum].status )
            {
                datagrams[seqNum].status = Sent;
                timestamp = rtt_ts(&rttInfo);
                BuildServerCommBuffer
                    (
                    sendBuf,
                    seqNum,
                    timestamp,
                    datagrams[seqNum].data
                    );
                //printf("Send Seq #%u, Timestamp %u: %s\n", atoi(sendBuf), timestamp, datagrams[seqNum].data );
                printf("Send Seq #%u, Timestamp %u.\n", seqNum, timestamp );
                Write( sockfd, sendBuf, CLIENT_DATAGRAM_TOTAL_SIZE );
                ++sentCount;

                if( 1 == sendFirstDatagramInWindow )
                {
                    //printf("=======================START RTO TIMER==========================\n");
                    sendFirstDatagramInWindow = 0;
                    StartTimer( &rttInfo );
                }
                continue;
            }

            if( Unused != datagrams[seqNum].status )
            {
                // has been sent; skip
                continue;
            }

            // Read (SERVER_CLIENT_DATAGRAM_PAYOLAD_SIZE-1) characters
            // and append '\0'.
            if( 1 == isEOFRead )
            {
                // Last time we have been read EOF.
                continue;
            }
            int nRead = fread( payload, sizeof(char), SERVER_CLIENT_DATAGRAM_PAYOLAD_SIZE - 1, fp );
            if( nRead == 0 || '\0' == payload[SERVER_CLIENT_DATAGRAM_PAYOLAD_SIZE-2] )
            {
                //printf("Last sequence number is read from the file\n");
                isEOFRead = 1;
                lastSeqNum = seqNum;
                //break;
            }
            else if( nRead < 0 )
            {
                err_sys("Error: read from file error\n");
            }

            payload[SERVER_CLIENT_DATAGRAM_PAYOLAD_SIZE] = '\0';
            timestamp = rtt_ts(&rttInfo);

            // Add datagram into server's buffer
            SetServerDatagram
                (
                &datagrams[seqNum],
                seqNum,
                timestamp,
                payload
                );

            // Build the sending buffer which will be sent to the client
            BuildServerCommBuffer
                (
                sendBuf,
                seqNum,
                timestamp,
                payload
                );

            //printf("Send Seq #%u, Timestamp %u: %s\n", atoi(sendBuf), timestamp, payload );
            printf("Send Seq #%u, Timestamp %u.\n", seqNum, timestamp );
            Write( sockfd, sendBuf, CLIENT_DATAGRAM_TOTAL_SIZE );
            ++sentCount;
            lastNRead = nRead;
            //IncrementBufferIndex( &seqNum, MAX_NUMBER_OF_BUFFER);

            // Start RTO timer
            if( 1 == sendFirstDatagramInWindow )
            {
                //printf("=======================START RTO TIMER==========================\n");
                sendFirstDatagramInWindow = 0;
                rtt_newpack( &rttInfo );
                StartTimer( &rttInfo );
            }
        }
        if( 0 == sentCount )
        {
            StartTimer( &rttInfo );
            printf("Receiver window is full. Lock!\n");
        }

        // Try to read from the client.
        const unsigned int ackReceived = ReadClientResponse
                                            (
                                            ackInfos,
                                            &ackInfoCount,
                                            &advertisedWndSize,
                                            &slidingWndConf,
                                            datagrams,
                                            &rttInfo,
                                            sockfd,
                                            g_pfd[PIPE_READ_FD]
                                            );

        //printf("ackReceived:%u\n", ackReceived);
        /*if( 0 == ackReceived )
        {
            continue;
        }*/

        //printf("isEOFRead %d, seq found %d\n", isEOFRead, SeqNumExistInAckInfo( lastSeqNum, ackInfos, ackInfoCount ));
        if( isEOFRead && 0 <= SeqNumExistInAckInfo( lastSeqNum, ackInfos, ackInfoCount ) )
        {
            //printf("Last ack is read from the client\n");
            break;
        }

        // clearStart is used to clear the datagrams that have been acked.
        unsigned int clearStart = (slidingWndConf.LAR + 1) % datagramCount;
        BOOL windowSlided = SlideWindow
                                (
                                &slidingWndConf,
                                ackInfos,
                                ackInfoCount
                                );

        if( windowSlided )
        {
            unsigned int clearEnd = (slidingWndConf.LAR );

            // Clear the datagrams between oldLAR and newLAR
            if( clearEnd >= clearStart )
            {
                unsigned int i = 0;
                for( i = clearStart; i <= clearEnd; ++i )
                {
                    ResetServerDatagram( &datagrams[i] );
                }
            }
            else
            {
                unsigned int i = 0;
                for( i = clearStart; i < datagramCount; ++i )
                {
                    ResetServerDatagram( &datagrams[i] );
                }
                for( i = 0; i <= clearEnd; ++i )
                {
                    ResetServerDatagram( &datagrams[i] );
                }
            }
        }

        // Update datagrams' status according to the ackInfos
        unsigned int i = 0;
        for( i = 0; i < ackInfoCount; ++i )
        {
            unsigned int seqNum = ackInfos[i].seqNumThatAcks;
            if( Sent == datagrams[seqNum].status  )
            {
                datagrams[seqNum].status = Acked;
            }
            else
            {
                // This mean the datagram is "clean"
                // Do nothing.
            }
        }

        //printf("advertisedWndSize: %d\n",advertisedWndSize);
        if( 0 == advertisedWndSize )
        {
            printf("Advertised wnd size = 0; Start persist timer.\n");
            StartPersistTimer();

            fd_set allrset;
            fd_set rset;
            FD_ZERO( &allrset );
            FD_ZERO( &rset );
            FD_SET( sockfd, &allrset );
            FD_SET( g_pfd[PIPE_READ_FD], &allrset );
            int maxfd = Max( sockfd, g_pfd[PIPE_READ_FD] );
            for( ; ; )
            {
                rset = allrset;
                int nReady = MySelect( maxfd+1, &rset, NULL, NULL, NULL );
                if( FD_ISSET( g_pfd[PIPE_READ_FD], &rset ) )
                {
                    // Persist time timeout event

                    // Just read 1 byte from the pipe and we don't process it.
                    char recvBuf[32];
                    read( g_pfd[PIPE_READ_FD], recvBuf, sizeof(recvBuf) );
                    printf("Persist timer timeout!\n");

                    char sendBuf[SERVER_DATAGRAM_TOTAL_SIZE];
                    bcopy ( WINDOW_PROBE_STRING, sendBuf, strlen(WINDOW_PROBE_STRING) );
                    Write( sockfd, sendBuf, CLIENT_DATAGRAM_TOTAL_SIZE );
                    StartPersistTimer();
                }

                if( FD_ISSET( sockfd, &rset ) )
                {
                    // We only care about the response to our window probe
                    // Other messages are discarded.
                    char recvBuf[SERVER_DATAGRAM_TOTAL_SIZE];
                    read( sockfd, &recvBuf, sizeof(recvBuf) );
                    if( 0 == bcmp( recvBuf, WINDOW_PROBE_STRING, strlen(WINDOW_PROBE_STRING) ) )
                    {
                        // This is triggered because we sent probe just now. Ignore this.
                        continue;
                    }
                    //printf("Read from Socket %s\n", recvBuf);
                    if( 0 == bcmp( recvBuf, "AWND:", 5 ) )
                    {
                        advertisedWndSize = atoi( recvBuf + 5 );
                        if(  0 != advertisedWndSize )
                        {
                            StopPersistTimer();
                            //StartTimer( &rttInfo );
                            break;
                        }
                    }
                }
            }
        }
    }

    printf( "File Transfer Complete\n" );

    fclose( fp );

    // Stop the timer and update the RTT as 0, since we no longer need it.
    StopTimer( &rttInfo, 0 );

    // Free the memory
    if( NULL != datagrams)
    {
        free( datagrams );
    }
    if( NULL != ackInfos)
    {
        free( ackInfos );
    }
}

const unsigned int ReadClientResponse
    (
    ackInfo *ackInfos,
    unsigned int *ackInfoCount,
    unsigned int *advertisedWndSize,
    slidingWindowConfig *wndConfig,
    serverDatagram *datagrams,
    struct rtt_info *rttInfo,
    int sockfd,
    int pipeReadFd
    )
{
    fd_set allrset;
    fd_set rset;
    FD_ZERO( &allrset );
    FD_ZERO( &rset );
    FD_SET( sockfd, &allrset );
    FD_SET( pipeReadFd, &allrset );
    int maxfd = Max( sockfd, pipeReadFd );
    char recvBuf[CLIENT_DATAGRAM_TOTAL_SIZE];

    unsigned int ackCount = 0;
    rset = allrset;        /* structure assignment */

    unsigned int ackReceived = 0;
    int nReady = MySelect( maxfd+1, &rset, NULL, NULL, NULL );
    if( FD_ISSET( sockfd, &rset ) )
    {
        while( 1 )
        {
            SetSocketNonBlocking( &sockfd );
            bzero( &recvBuf, CLIENT_DATAGRAM_TOTAL_SIZE );
            int nRecv = read( sockfd, &recvBuf, sizeof(recvBuf) );
            //printf("nRecv: %d\n", nRecv);
            if( -1 == nRecv )
            {
                //printf( "No more acks.\n" );
                break;
            }

            if( 0 == bcmp( recvBuf, "AWND:", 5 ) )
            {
                // Don't process this message
                continue;
            }

            unsigned int ackNum = 0;
            unsigned int seqNum = 0;
            *advertisedWndSize = 0;
            uint32_t timestamp = 0;
            SetSocketBlocking( &sockfd );
            ParseClientDatagram( &ackNum , advertisedWndSize, &timestamp, recvBuf );
            if( 0 == ackNum )
            {
                seqNum = wndConfig->bufferSize - 1;
            }
            else
            {
                seqNum = ackNum - 1;
            }

            CongestionControl
                    (
                    &g_cwnd,
                    &g_ssthresh,
                    &g_state,
                    &g_dupAckCount,
                    g_thisAck,
                    &g_lastAckReceived,
                    &datagrams[(wndConfig->LAR + 1) % wndConfig->bufferSize]
                    );

            printf("Read Ack #%u, AWND: %u, timestamp: %u\n", ackNum , *advertisedWndSize, timestamp );
            if( IsSeqNumInSlidingWindow( wndConfig, seqNum ) )
            {
                //printf( "(Inside the window)" );
                // If the data's seqence number corresponding to the received
                // ack is inside the sliding window, then add into ack info.
                //printf("AddAckInfo index %d, seqNum %d, time %d\n", ackCount, seqNum, timestamp);
                AddAckInfo( ackInfos, &ackCount, seqNum, timestamp );
                g_thisAck = (seqNum + 1 ) % wndConfig->bufferSize;

                // Here, LAR has not been updated, so sequence number
                // indexed by (LAR+1) is associated with the first datagram
                // in the sender sliding window.
                const int matchedIndex = SeqNumExistInAckInfo
                                            (
                                            ( wndConfig->LAR + 1) % wndConfig->bufferSize,
                                            ackInfos,
                                            ackCount
                                            );
                //printf("matchedIndex (%d)\n",matchedIndex);
                if( 0 <= matchedIndex )
                {
                    //printf("=======================STOP RTO TIMER==========================\n");
                    StopTimer( rttInfo, ackInfos[matchedIndex].timestamp );
                }
                ackReceived = 1;
            }
            printf( "\n" );
        }
    }

    if( FD_ISSET( pipeReadFd, &rset ) )
    {
        // RTO Timer
        if( rtt_timeout( rttInfo ) < 0 )
        {
            err_sys( "No response from client. Give up." );
            errno = ETIMEDOUT;
            return (-1);
        }
        char recvBuf[32];
        read( g_pfd[PIPE_READ_FD], recvBuf, sizeof(recvBuf) );

        time_t ticks= time(NULL);
        printf("RTO timer timeout: %24s", ctime( &ticks ) );

        // Modify the FIRST datagram's status so that next time
        // it can be re-transmitted.
        datagrams[(wndConfig->LAR + 1) % wndConfig->bufferSize].status = NeedRetransmitting;
        //StartTimer( rttInfo );

        g_RTOTimerTimeout = TRUE;
    }

    *ackInfoCount = ackCount;
    return ackReceived;
}

void SetServerDatagram
    (
    serverDatagram *datagram,
    unsigned int seqNum,
    uint32_t timestamp,
    char *payload
    )
{
    datagram->status = 1;
    datagram->seqNum = seqNum;
    datagram->timestamp = timestamp;
    bzero( &datagram->data, SERVER_CLIENT_DATAGRAM_PAYOLAD_SIZE );
    bcopy( payload, &datagram->data, SERVER_CLIENT_DATAGRAM_PAYOLAD_SIZE );
}

void ResetServerDatagram( serverDatagram *datagram )
{
    datagram->status = 0;
    datagram->seqNum = 0;
    datagram->timestamp = 0;
    bzero( &datagram->data, SERVER_CLIENT_DATAGRAM_PAYOLAD_SIZE );
}

void InitSlidingWindowConfig
    (
    slidingWindowConfig *slidingWndConf,
    const unsigned int bufSize,
    const unsigned int wndSize
    )
{
    slidingWndConf->bufferSize = bufSize;
    slidingWndConf->LAR = bufSize - 1;
    slidingWndConf->LFS = wndSize - 1;
}

int IsSeqNumInSlidingWindow
    (
    slidingWindowConfig * wndConfig,
    unsigned int seqNum
    )
{
    //printf("(IsSeqNumInSlidingWindow) LAR %d, LFS %d, seqNum %d\n", wndConfig->LAR, wndConfig->LFS, seqNum );
    if( wndConfig->LAR == wndConfig->LFS )
    {
        err_sys( "Error. LAR should not be equal to LFS." );
    }

    if( wndConfig->LAR == wndConfig->bufferSize )
    {
        if( seqNum >=0 && seqNum <= wndConfig->LFS )
        {
            return 1;
        }
    }

    if( wndConfig->LAR < wndConfig->LFS )
    {
        if( seqNum > wndConfig->LAR && seqNum <= wndConfig->LFS )
        {
            return 1;
        }
    }

    if( wndConfig->LAR > wndConfig->LFS )
    {
        if( seqNum > wndConfig->LAR || seqNum <= wndConfig->LFS )
        {
            return 1;
        }
    }

    return 0;
}

void AddAckInfo
    (
    ackInfo *ackInfos,
    unsigned int *ackCount,
    const unsigned int seqNum,
    const uint32_t timestamp
    )
{
    ackInfos[*ackCount].seqNumThatAcks = seqNum;
    ackInfos[*ackCount].timestamp = timestamp;
    //printf("AddAckInfo: index=%d, seq=%d\n", *ackCount, ackInfos[*ackCount].seqNumThatAcks);
    *ackCount = *ackCount + 1;
}

BOOL  SlideWindow
    (
    slidingWindowConfig *slidingWndConf,
    ackInfo *ackInfos,
    const unsigned int ackInfoCount
    )
{
    if( 0 == ackInfoCount )
    {
        // We don't receive any ack. So no need to slide the window
        return FALSE;
    }

    unsigned int oldLAR = slidingWndConf->LAR;

    // Here, we are sure that all seqence numbers in the
    // ackInfo are within the sliding window because
    // we have called IsSeqNumInSlidingWindow before.
    unsigned int largestSeqNum = FindLargestSeqNum
                                        (
                                        slidingWndConf->LAR,
                                        slidingWndConf->LFS,
                                        slidingWndConf->bufferSize,
                                        ackInfos,
                                        ackInfoCount
                                        );
    // Update LAR
    slidingWndConf->LAR = largestSeqNum;
    //printf("In SlidingWindow: old %u, new %d\n", oldLAR, largestSeqNum);
    if( slidingWndConf->LAR == oldLAR )
    {
        return FALSE;
    }
    else
    {
        return TRUE;
    }
}

unsigned int
FindLargestSeqNum
    (
    const unsigned int LAR,
    const unsigned int LFS,
    const unsigned int datagramCount,
    ackInfo *ackInfos,
    const unsigned int ackInfoCount
    )
{
    //printf("FindLargestSeqNum: %d\n", ackInfos[0].seqNumThatAcks);
    //printf("LAR %d, LFS %d, ackInfoCount %d\n", LAR, LFS, ackInfoCount );
    unsigned int largestSeqNum = ( LAR + 1 ) % datagramCount;
    if( LAR == ( datagramCount-1 ) || LAR < LFS )
    {
        unsigned int i = LFS;
        const unsigned lowerBound = ( LAR + 1 ) % datagramCount;
        for( ; i >= lowerBound; --i )
        {
            //printf("FindLargestSeqNum: i=%d\n",i);
            if( 0 <= SeqNumExistInAckInfo( i, ackInfos, ackInfoCount ) )
            {
                largestSeqNum = i;
                break;
            }
        }
    }
    else
    {
        // Yes, we should use integer i instead of unsigned integer i.
        // Otherwise, when i = 0 and decrement it, we will get an
        // unreasonably large value.
        int i = LFS;
        unsigned int shouldExit = 0;
        for( ; !shouldExit && i >= 0; --i )
        {
            if( 0 <= SeqNumExistInAckInfo( i, ackInfos, ackInfoCount ) )
            {
                largestSeqNum = i;
                shouldExit = 1;
            }
        }

        i = datagramCount - 1;
        for( ; !shouldExit && i > LAR; --i )
        {
            if( 0 <= SeqNumExistInAckInfo( i, ackInfos, ackInfoCount ) )
            {
                largestSeqNum = i;
                shouldExit = 1;
            }
        }
    }

    //printf("largestSeqNum %d\n",largestSeqNum);
    return largestSeqNum;
}

const int SeqNumExistInAckInfo
    (
    const unsigned int key,
    ackInfo *ackInfos,
    const unsigned int ackInfoCount
    )
{
    unsigned int i = 0;
    //printf("(SeqNumExistInAckInfo) key(%d), ackCount(%d)\n", key, ackInfoCount);
    for( i = 0; i < ackInfoCount; ++i )
    {
        //printf("(SeqNumExistInAckInfo) i=%d key=%d, seq=%d\n", i, key, ackInfos[i].seqNumThatAcks );
        if( key == ackInfos[i].seqNumThatAcks )
        {
            return i;
        }
    }

    // Not found
    return -1;
}

void CongestionControl
    (
    unsigned int *pCwnd,
    unsigned int *pssthresh,
    congestionCtrlState *pState,
    unsigned int *pDuplicateCount,
    unsigned int thisAck,
    int *pLastAckReceived,
    serverDatagram *datagram
    )
{
    static const unsigned int dupCountTriggeringFastRecovery = 3;
    unsigned int cwnd = *pCwnd;
    unsigned int ssthresh = *pssthresh;
    congestionCtrlState state =*pState;
    unsigned int duplicateCount = *pDuplicateCount;
    unsigned int lastAckReceived = *pLastAckReceived;

    // Finite state machine
    {
        BOOL isNewAcked = thisAck == lastAckReceived ? FALSE : TRUE;

        switch( state )
        {
        case SlowStart:
            if( g_RTOTimerTimeout )
            {
                g_RTOTimerTimeout = FALSE;
                ssthresh = cwnd >> 1;
                // ssthresh should be at least two segments
                ssthresh = Max( 2, ssthresh );
                cwnd = 1;
                duplicateCount = 0;
            }
            else
            {
                if( isNewAcked )
                {
                    cwnd = cwnd << 1;
                    duplicateCount = 0;
                }
                else
                {
                    if( dupCountTriggeringFastRecovery <= duplicateCount )
                    {
                        cwnd = cwnd >> 1;
                        ssthresh = ssthresh + 3;
                        state = FastRecovery;
                        printf("(Congestion Control )Change to FastRecovery\n");
                        datagram->status = NeedRetransmitting;
                    }
                    else
                    {
                        ++duplicateCount;
                    }
                }
            }

            if( cwnd >= ssthresh )
            {
                printf("(Congestion Control )Change to CA\n");
                state = CongestionAvoidance;
            }
            break;
        case CongestionAvoidance:
            if( g_RTOTimerTimeout )
            {
                printf("(Congestion Control )Change to SlowStart\n");
                g_RTOTimerTimeout = FALSE;
                ssthresh = cwnd >> 1;
                ssthresh = Max( 2, ssthresh );
                cwnd = 1;
                duplicateCount = 0;
                state = SlowStart;
            }
            else
            {
                if( isNewAcked )
                {
                    ++cwnd;
                    duplicateCount = 0;
                }
                else
                {
                    if( dupCountTriggeringFastRecovery <= duplicateCount )
                    {
                        ssthresh = cwnd >> 1;
                        cwnd = ssthresh + 3;
                        state = FastRecovery;
                        printf("(Congestion Control )Change to FastRecovery\n");
                        datagram->status = NeedRetransmitting;
                    }
                    else
                    {
                        ++duplicateCount;
                    }
                }
            }
            break;
        case FastRecovery:
            if( g_RTOTimerTimeout )
            {
                g_RTOTimerTimeout = FALSE;
                ssthresh = cwnd >> 1;
                ssthresh = Max( 2, ssthresh );
                cwnd = 1;
                duplicateCount = 0;
                printf("(Congestion Control )Change to SlowStart\n");
                state = SlowStart;
            }
            else
            {
                if( isNewAcked )
                {
                    cwnd = ssthresh;
                    duplicateCount = 0;
                }
                else
                {
                    ++cwnd;
                }
            }
            break;
        default:
            err_msg("I really don't understand why you're here...");
        }
    }

    if( SlowStart == state )
    {
        printf( "state: slow start, ");
    }
    else if( CongestionAvoidance== state )
    {
        printf( "state: congestion avoidance, ");
    }
    else if( FastRecovery== state )
    {
        printf( "state: fast recovery, ");
    }
    printf("currentAck (%d), lastAckReceived (%d), dupCount(%d).\n", thisAck, lastAckReceived, duplicateCount);
    printf("cwnd (%d), ssthresh (%d)\n", cwnd, ssthresh);

    *pCwnd = cwnd;
    *pssthresh = ssthresh;
    *pState = state;
    *pDuplicateCount = duplicateCount;
    *pLastAckReceived = g_thisAck;
}

void DebugConnectionRecords
    (
    connectionRecord *connRecords,
    const unsigned int currentMaxNumOfConnRecords
    )

{
#ifdef DEBUG_SERVER_TASKS_DETAILED
    printf( "==========Connection Records===========\n" );
    printf("The number of current used record: %d\n", currentMaxNumOfConnRecords );
    unsigned int i = 0;
    for( i = 0; i <MAX_CONNECTION_RECORD; ++i )
    {
        printf( "PID: %d, Address: %s\n", connRecords[i].pid, connRecords[i].clientAddrPort );
    }
    printf( "==================END==================\n" );
#endif
}

void DetermineServClientRelation
    (
    srvCliRelation *relation,
    char *servAddr,
    char *mask,
    char *subnetAddr,
    char *cliAddr
    )
{
    if( 0 == bcmp( servAddr, cliAddr, MAXLINE ) )
    {
        // Both IPServer and IPClient are loopback address.
        *relation = LoopBack;
    }

    char copyOfClientIPAddr[MAXLINE];
    char copyOfMask[MAXLINE];
    char copyOfSubnetAddr[MAXLINE];
    bzero( copyOfClientIPAddr, MAXLINE );
    bzero( copyOfMask, MAXLINE );
    bzero( copyOfSubnetAddr, MAXLINE );
    bcopy( cliAddr, copyOfClientIPAddr, MAXLINE );
    bcopy( mask, copyOfMask, MAXLINE );
    bcopy( subnetAddr, copyOfSubnetAddr, MAXLINE );

    char *pStr1 = copyOfClientIPAddr;
    char *pStrSubnet = copyOfSubnetAddr;
    char *pStrMask = copyOfMask;
    char token1[MAXLINE];
    char tokenSubnet[MAXLINE];
    char tokenMask[MAXLINE];

    while( 1 )
    {
        pStr1 = GetToken( pStr1, token1 );
        pStrSubnet = GetToken( pStrSubnet, tokenSubnet );
        pStrMask = GetToken( pStrMask, tokenMask );

        const int subnetIDSeg1 = atoi(token1) & atoi(tokenMask);
        const int subnetIDSeg2 = atoi(tokenSubnet);
    #ifdef DEBUG_SERVER_TASKS
        printf("Mask: %s, Client:%s, Server:%s, ClientSubID:%d, ServerSubID:%d.\n", tokenMask, token1, token2, subnetIDSeg1, subnetIDSeg2);
    #endif
        if( subnetIDSeg1 != subnetIDSeg2 )
        {
            *relation = NotLocal;
            break;    // We are sure that client and server are not local.
        }
        else
        {
            *relation = Local; // "May be" local, so we still have to check.
        }

        if( '\0' == pStr1[0] || '\0' == pStrSubnet[0] || '\0' == pStrMask[0] )
        {
            // We have check all IP segment
            // We define IP segment which is splited by the '.' delimiter.
            break;
        }
    }
}


void DebugServerRecords
    (
    networkInterfaceRecord *records,
    const unsigned int numOfNetworkRecords
    )
{
    unsigned int i;
    printf( "=======Network Interface Summary=======\n" );
    for( i = 0; i < numOfNetworkRecords; ++i )
    {
        printf( "Record %u:\n", i + 1 );
    #ifdef DEBUG_SERVER_TASKS
        printf( "\tSocket fd %d:\n", records[i].sockfd );
    #endif
        printf( "\tThe IP addr: %s.\n", records[i].IPAddr );
        printf( "\tThe network mask addr: %s.\n", records[i].mask );
        printf( "\tThe subnet addr: %s.\n", records[i].subnetAddr );
    }
    printf( "==================END==================\n" );
}

const unsigned int Min( const unsigned int a, const unsigned int b )
{
    return (a>b)? b:a;
}

const unsigned int Max( const unsigned int a, const unsigned int b )
{
    return (a>b)? a:b;
}

void IncrementBufferIndex( unsigned int *index, int maxSize )
{
    *index = *index + 1;
    *index = *index % maxSize;
}

void SetMSGPeek( int *flag )
{
    (*flag) |= (MSG_PEEK);
}

void ResetMSGPeek( int *flag )
{
    (*flag) &= (~MSG_PEEK);
}

void SetSocketNonBlocking( int *sockfd )
{
    int flags;
    if( ( flags = fcntl( *sockfd, F_GETFL, 0 ) ) < 0 )
    {
        // If error occurs, we print the error message and exit.
        err_sys( "F_GETFL error.\n" );
    }

    // OK, we have successfully got the flags.
    // Let's set our own desired flags.
    flags |= O_NONBLOCK;
    if( fcntl( *sockfd, F_SETFL, flags ) < 0 )
    {
        // If error occurs, we print the error message and exit.
        err_sys( "F_SETFL error.\n" );
    }
}

void SetSocketBlocking( int *sockfd )
{
    int flags;
    if( ( flags = fcntl( *sockfd, F_GETFL, 0 ) ) < 0 )
    {
        // If error occurs, we print the error message and exit.
        err_sys( "F_GETFL error.\n" );
    }

    // OK, we have successfully got the flags.
    // Let's set our own desired flags.
    flags &= (~O_NONBLOCK);
    if( fcntl( *sockfd, F_SETFL, flags ) < 0 )
    {
        // If error occurs, we print the error message and exit.
        err_sys( "F_SETFL error.\n" );
    }
}

static void sigAlarmHandler( int signo )
{
    // Calling stardard IO functions in a signal handler is not
    // recommended. See textbook section 11.18.
    // Uncomment this only for diagnostic purpose.
    //printf(" In sigAlarmHandler\n" );

    // For RTO timeout or persist timer timeout
    // Write one byte into pipe.
    Write( g_pfd[PIPE_WRITE_FD], "1", 1 );
}

